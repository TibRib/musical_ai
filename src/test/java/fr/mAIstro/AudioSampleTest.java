package fr.mAIstro;

import fr.mAIstro.audio.Music;
import fr.mAIstro.audio.Note;
import junit.framework.TestCase;
import org.junit.Test;
import static org.junit.Assert.*;

public class AudioSampleTest extends TestCase {

    public static void testeFFT440(){
        Music piste440 = new Music("./src/main/resources/music/440Hz_tonalite.wav");
        piste440.associeNotes();
        piste440.displayNotes();
        Note note5 = piste440.getNotes().get(20);
        assertEquals("verification La","la",note5.getName());
        assertEquals(" verification Octave 3",3,note5.getOctave());
    }

    public static void testeFFTDual440(){
        Music piste440 = new Music("./src/main/resources/music/la440_dual.wav");
        piste440.associeNotes();
        piste440.displayNotes();
        Note note5 = piste440.getNotes().get(20);
        assertEquals("verification La","la",note5.getName());
        assertEquals(" verification Octave 3",3,note5.getOctave());
    }

    public static void testeFFT600(){
        Music piste600 = new Music("./src/main/resources/music/600Hz_tonalite.wav");
        piste600.associeNotes();
        piste600.displayNotes();
        Note note5 = piste600.getNotes().get(20);
        assertEquals("verification La","ré",note5.getName());
        assertEquals(" verification Octave 3",4,note5.getOctave());
    }



}
