package fr.mAIstro;


import fr.mAIstro.UI.Panels.AcceuilPanel;
import fr.mAIstro.UI.Panels.NeuralNetworkPane;
import fr.mAIstro.UI.Panels.SelectionPanel;
import fr.mAIstro.UI.Panels.VisualizerPanel;
import fr.mAIstro.ai.neuralnetwork.Network;
import fr.mAIstro.ai.neuralnetwork.NetworkPanel;
import fr.mAIstro.ai.neuralnetwork.TrainingSet;
import fr.mAIstro.ai.neuralnetwork.implementations.AI_Genre;
import fr.mAIstro.ai.neuralnetwork.implementations.AI_Instrument;
import fr.mAIstro.ai.neuralnetwork.implementations.AI_Shmilblick;
import fr.mAIstro.audio.AudioSample;
import fr.mAIstro.audio.Music;
import fr.mAIstro.audio.Note;
import fr.mAIstro.audio.Playlist;
import fr.mAIstro.lib.FileService;
import javafx.application.Application;
import javafx.application.Platform;
import javafx.scene.Scene;
import javafx.scene.chart.*;
import javafx.stage.Stage;
import javafx.stage.StageStyle;

import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;


public class App extends Application {

    private static int curCpt=0;

    @Override
    public void start(Stage stage) throws Exception {
        stage.initStyle(StageStyle.UNDECORATED);
        AcceuilPanel a = new AcceuilPanel(stage);
    }

    public static void main(String[] args) {
        //testeFFT();
        //testAIMultilayer();
        //testVisualTraining();

        //testAI_Genre();
        //testAI_Instruments();
        //testVisualTraining();
        //testAI_Genre();

        /*
        //LA RECUPERATION DES HARMONIQUES CEST ICI !
        harmoniquesEchantillon("./src/main/resources/music/clair_lune_piano_mono.wav");
        echantillon("./src/main/resources/music/clair_lune_piano_mono.wav");
        harmoniquesEchantillon("./src/main/resources/music/clarinet_A3_1_piano_normal.wav");
        //Verification des octaves
        echantillon("./src/main/resources/music/clair_lune_piano_mono.wav");
        */
        
        launch(args);
        System.exit(0);
    }

    private static void testAI_Instruments() {
        float pPiano = 0;
        float pGuitar = 0;
        float pSax = 0;
        float pViolon = 0;
        float pFlute = 0;
        float pTot = 0;
        AI_Instrument ai = new AI_Instrument("./src/main/resources/SavedNeuralNetworks/IA_instruments.txt");
        Music m = new Music("./src/main/resources/music/128_D#m_DreamySub_01_726.wav");
        m.associeNotes();
        for(AudioSample sample : m.getSamples()) {
            if(!sample.isSilent() && sample.getFrequenceObtenue() != 0) {
                ArrayList<Float> inputs = new ArrayList<>();
                for(float f : sample.getHarmoniques())
                    inputs.add(f);
                ArrayList<Float> answers = ai.calculateWithNetwork(inputs);
                pPiano += answers.get(0);
                pGuitar += answers.get(1);
                pSax += answers.get(2);
                pViolon += answers.get(3);
                pFlute += answers.get(4);
                pTot++;
            }
        }
        System.out.println("Piano: "+pPiano/pTot);
        System.out.println("Piano: "+pGuitar/pTot);
        System.out.println("Piano: "+pSax/pTot);
        System.out.println("Piano: "+pViolon/pTot);
        System.out.println("Piano: "+pFlute/pTot);
/*
        TrainingSet trainingSet = new TrainingSet("./src/main/resources/SavedTrainingSets/TrainingSetAi_instruments.txt");
        AI_Instrument ai = new AI_Instrument();
        ai.setTraining_set(trainingSet);
        ai.getTraining_set().save("TrainingSetAi_instruments");
        ai.apprentissage();
        ai.save("AI_instruments");
*/

/*
        AI_Instrument ai = new AI_Instrument("./src/main/resources/SavedNeuralNetworks/IA_instruments.txt");

        String musicfolderpath = "./src/main/resources/music/instruments/";
        ArrayList<String> instrumentslist = new ArrayList<>();
        instrumentslist.add("flute");
        instrumentslist.add("guitar");
        instrumentslist.add("piano");

        float pFlute = 0;
        float pGuitar = 0;
        float pSax = 0;
        int tot = 0;
        int bonnerep = 0;
        int totrep = 0;
        int cptInstrument = 0;
        for(String strInstrument : instrumentslist) {
            System.out.println("CHANGEMENT INSTRUMENT "+strInstrument);
            for (String filename : FileService.getListFile(musicfolderpath + strInstrument + "/", "wav")) {
                tot++;
                Music music = new Music(musicfolderpath + strInstrument + "/" + filename);
                music.associeNotes();
                System.out.println("LOAD "+musicfolderpath + strInstrument + "/" + filename);
                for(AudioSample sample : music.getSamples())
                {
                    if(!sample.isSilent() && sample.getFrequenceObtenue() != 0 && sample.getHarmoniques().length == 5) {
                        totrep++;
                        ArrayList<Float> inputs = new ArrayList<>();
                        for(Float harm : sample.getHarmoniques())
                            inputs.add(harm);
                        ArrayList<Float> rep = ai.calculateWithNetwork(inputs);
                        if(cptInstrument == 0)
                        {
                            if(rep.get(0) >= 0.9)
                                bonnerep++;
                            else
                                System.out.println("error on : "+inputs +" sortie :" +rep);
                        }
                        else if(cptInstrument == 1)
                        {
                            if(rep.get(1) >= 0.9)
                                bonnerep++;
                            else
                                System.out.println("error on : "+inputs +" sortie :" +rep);
                        }
                        else if(cptInstrument == 2)
                        {
                            if(rep.get(2) >= 0.9)
                                bonnerep++;
                            else
                                System.out.println("error on : "+inputs +" sortie :" +rep);
                        }
                        else if(cptInstrument == 3)
                        {
                            if(rep.get(3) >= 0.9)
                                bonnerep++;
                            else
                                System.out.println("error on : "+inputs +" sortie :" +rep);
                        }
                        else if(cptInstrument == 4)
                        {
                            if(rep.get(4) >= 0.9)
                                bonnerep++;
                            else
                                System.out.println("error on : "+inputs +" sortie :" +rep);
                        }
                    }
                }

                //System.gc();
            }
            cptInstrument++;
        }
        System.out.println(bonnerep + "/"+totrep);*/
    }


    private static void testAI_Genre(){
        AI_Genre ai = new AI_Genre();
        ai.apprentissage();
        Music music = new Music("../samples/Jazz/bebop.wav");
        System.out.println(ai.calculateWithNetwork(music.getDataAI_Genre()));
        ai.save("AI_Genre");
    }

    private static void testVisualTraining() {
        AI_Shmilblick ai = new AI_Shmilblick();

        NetworkPanel panel = new NetworkPanel(ai);
        panel.appearAsWindow();

        //ai.apprentissage(panel,0);
    }

    private static void testAIMultilayer() {
        AI_Shmilblick network = new AI_Shmilblick();
        network.apprentissage();

        ArrayList<Float> entree = new ArrayList<Float>();
        entree.add(1f);
        entree.add(1f);
        entree.add(1f);
        System.out.println(network.calculateWithNetwork(entree));

        network.save("multilayer_shmilblik");
        System.out.println(network.calculateWithNetwork(entree));

        Network network2 = new AI_Shmilblick("./src/main/resources/SavedNeuralNetworks/multilayer_shmilblik.txt");
        System.out.println(network2.calculateWithNetwork(entree));
    }

    private static void joueClair2lune(){
        List<Note> clair2lune = new ArrayList<Note>();
        clair2lune.add(new Note("do",3));
        clair2lune.add(new Note("do",3));
        clair2lune.add(new Note("do",3));
        clair2lune.add(new Note("ré",3));
        clair2lune.add(new Note("mi",3));
        clair2lune.add(new Note("ré",3));
        clair2lune.add(new Note("do",3));
        clair2lune.add(new Note("mi",3));
        clair2lune.add(new Note("ré",3));
        clair2lune.add(new Note("ré",3));
        clair2lune.add(new Note("do",3));

        for ( Note note: clair2lune) {
            note.play(44100,130);
        }
    }

    private static void harmoniquesEchantillon(String nomFichier) {
        Music m = new Music(nomFichier);
        m.associeNotes(2048);
        System.out.println(m.getNotes().size() + " notes enregistrees");
        m.displayNotes();
        //System.out.println("Frequence d'apparition du ré : " + m.getPercentage("ré"));
        //System.out.println("Octave moyen du ré : " + m.getOctaveMoyen("ré"));

        /* Recuperation des echantillons 1 par 1 pour afficher les harmoniques et la fondamentale */
        for (AudioSample sample: m.getSamples()) {
            //Clé
            double serial =0;
            if(sample.isSilent() == false) {

                //Harmoniques
                float[] f = sample.getHarmoniques();
                System.out.println("Fondamentale f0 (" + (sample.getFrequenceObtenue()) + ")");
                System.out.println("Module f0 = " + (sample.getModFondamentale()) );

                for (int i = 1; i <= f.length; i++) {
                    System.out.println("Harmonique f" + (i) + " (" + (sample.getFrequenceObtenue() * +(i + 1)) + ") = " + f[i-1]);
                    serial += f[i-1] * f[i-1];
                }
            }
            serial = Math.sqrt(serial);

            System.out.println("Clé renvoyée : " +serial);
        }

        //Ici c'est simplement l'affichage des harmoniques : pour chaque dixieme du morceau si sample non vide

        for (int i = 0; i < 10; i+=1) {
            AudioSample sample = m.getSamples()[i*(m.getSamples().length/10)];
            if (sample.isSilent() == false) {
                Stage stage = new Stage();
                stage.setTitle("Line Chart Sample");
                //defining the axes
                final CategoryAxis xAxis = new CategoryAxis();
                final NumberAxis yAxis = new NumberAxis();
                xAxis.setLabel("Frequences et amplitudes");
                //creating the chart
                final BarChart<String, Number> bc = new BarChart<String, Number>(xAxis, yAxis);
                bc.setTitle("Représentation Frequentielle");
                xAxis.setLabel("Harmonique");
                yAxis.setLabel("Amplitude");
                xAxis.setAnimated(false);
                yAxis.setAnimated(false);

                bc.setTitle("Fréquences de l'echantillon analysé");
                //defining a series
                XYChart.Series harm = new XYChart.Series();

                Scene scene = new Scene(bc, 800, 600);

                if (sample.isSilent() == false) {
                    //Harmoniques
                    float[] f = sample.getHarmoniques();
                    harm.getData().clear();
                    harm.getData().add(new XYChart.Data("" + (sample.getFrequenceObtenue()), sample.getModFondamentale()));
                    for (int j = 1; j <= f.length; j++) {
                        harm.getData().add(new XYChart.Data("" + (sample.getFrequenceObtenue() * (j + 1)), f[j - 1]));
                    }
                }

                bc.getData().addAll(harm);
                stage.setScene(scene);
                stage.show();
            }
        }
    }

    private static void echantillon(String nomFichier) {
        Music m = new Music(nomFichier);
        m.associeNotes(2048);
        System.out.println(m.getNotes().size() + " notes enregistrees");
        m.displayNotes();
        float[] octaves = m.getTabAvgOctaveNotes();
        float[] frequences = m.getTabFrequencesNotes();
        for (int i = 0; i < 12; i++) {
            System.out.println("Frequence d'apparition du "+i+" : " + frequences[i]);
            System.out.println("Octave moyen du "+ i +" : "+ octaves[i]);
        }
    }

    private static void FFTplot(String nomFichier) {
        Music m = new Music(nomFichier);
        m.associeNotes(2048);

            /* Creation d'une fenetre */
            Stage stage = new Stage();
            stage.setTitle("Line Chart Sample");
            //defining the axes
            final NumberAxis xAxis = new NumberAxis();
            xAxis.setAnimated(false);
            final NumberAxis yAxis = new NumberAxis();
            yAxis.setAnimated(false);
            xAxis.setForceZeroInRange(false);
            yAxis.setAutoRanging(false);
            yAxis.setForceZeroInRange(false);
            xAxis.setLabel("Frequences et amplitudes");
            //creating the chart
            final AreaChart<Number, Number> lineChart =
                    new AreaChart<>(xAxis, yAxis);

            lineChart.setTitle("Fréquences de l'echantillon analysé");
            //defining a series
            XYChart.Series energies = new XYChart.Series();

            Scene scene = new Scene(lineChart, 800, 600);
            curCpt = 0;

            ScheduledExecutorService scheduledExecutorService = Executors.newSingleThreadScheduledExecutor();

            scheduledExecutorService.scheduleAtFixedRate(() -> {
                Platform.runLater(() -> {
                    m.getSamples()[curCpt].setSerieGraphique(energies);
                    curCpt++;
                    if(curCpt>= m.getSamples().length) curCpt = 0;
                });
            }, 0, 500, TimeUnit.MILLISECONDS);


            lineChart.getData().addAll(energies);
            lineChart.setCreateSymbols(false);

            stage.setScene(scene);
            stage.show();
    }
}

