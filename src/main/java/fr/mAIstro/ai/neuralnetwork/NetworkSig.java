package fr.mAIstro.ai.neuralnetwork;

public abstract class NetworkSig extends Network{
    public NetworkSig(float eta, float toleranceSortie, int nb_entree, int nbNeuronesCoucheEntree, int nbCouchesCachees, int nb_sortie) {
        super(eta, toleranceSortie, nb_entree, nbNeuronesCoucheEntree, nbCouchesCachees, nb_sortie);
    }

    public NetworkSig(String filename) {
        super(filename);
    }
}
