package fr.mAIstro.ai.neuralnetwork;

import fr.mAIstro.UI.Panels.NetworkCanvas;
import fr.mAIstro.ai.neuron.Neuron;
import fr.mAIstro.lib.FileService;

import java.io.File;
import java.io.FileWriter;
import java.util.ArrayList;
import java.util.Arrays;

public abstract class Network {
    // Critère vitesse apprentissage, commun à tous les neurones
    private float eta = 0.15f;
    // Tolérance générique permettant d'accepter la sortie d'un neurone comme valable
    private float toleranceSortie = 0.01f;
    //Booléen signifiant si le réseau a finit l'entrainement
    private boolean estApprentissageFinis = false;

    // Tableau 2D (réseau multi-couche)
    private ArrayList<ArrayList<Neuron>> couchesCachees = null;

    //Liste des sorties
    private ArrayList<Neuron> coucheSortie = null;
    private TrainingSet training_set = null;
    private ArrayList<String> output_name = new ArrayList<>();

    /**
     * Constructeur semi-avancé permettant de paramétrer la vitesse d’apprentissage et la tolérance des sorties par rapport aux résultats attendus
     * @param eta
     * @param toleranceSortie
     * @param nb_entree
     * @param nb_sortie
     */
    public Network(float eta, float toleranceSortie, int nb_entree,int nbNeuronesCoucheEntree,int nbCouchesCachees, int nb_sortie) {
        this.couchesCachees = new ArrayList<ArrayList<Neuron>>();

        //Fabrication couche entrée
        ArrayList<Neuron> coucheEntree = new ArrayList<Neuron>();
        for(int idCouche=0;idCouche<nbNeuronesCoucheEntree;idCouche++)
            coucheEntree.add(new Neuron(nb_entree));
        this.couchesCachees.add(coucheEntree);

        //Fabrication couche cachée
        for(int idCouche=0; idCouche<nbCouchesCachees;idCouche++)
        {
            ArrayList<Neuron> couche = new ArrayList<Neuron>();
            //Chaque couche cachées contient 0.75x Neurones que la couche d'entrée cf(Venugopal et Baets, 1994).
            for(int idNeurone=0;idNeurone<(int) (0.75*coucheEntree.size());idNeurone++)
            {
                if(idCouche == 0)
                    couche.add(new Neuron(coucheEntree.size()));
                else
                    couche.add(new Neuron((int) (0.75*coucheEntree.size())));
            }
            this.couchesCachees.add(couche);
        }

        //Fabrication couche sortie
        this.coucheSortie = new ArrayList<Neuron>();
        for(int idNeurone=0;idNeurone<nb_sortie;idNeurone++)
            this.coucheSortie.add(new Neuron(couchesCachees.get(couchesCachees.size()-1).size()));

        this.eta = eta;
        this.toleranceSortie = toleranceSortie;

        //Appelle la méthode pour fabriquer le set d'entrainement
        this.setTraining_set(makeTrainingSet());
    }

    //Charge un réseau RNN depuis un fichier
    public Network(String filename){
        couchesCachees = new ArrayList<>();
        coucheSortie = new ArrayList<>();
        String filecontents = FileService.getFileContents(filename);
        eta = Float.parseFloat(filecontents.split("eta=")[1].split(",")[0]);
        toleranceSortie = Float.parseFloat(filecontents.split("toleranceSortie=")[1].split(",")[0]);
        estApprentissageFinis = Boolean.parseBoolean(filecontents.split("estApprentissageFinis=")[1].split(",")[0]);

        ArrayList<String> str_list_coucheCache = new ArrayList<String>(Arrays.asList(filecontents.split("couchesCachees=\\[")[1].split("coucheSortie=")[0].split("], \\[")));
        for(String str_no_split : str_list_coucheCache){
            ArrayList<String> str_coucheCache = new ArrayList<>(Arrays.asList(str_no_split.split("Neurone")));
            str_coucheCache.remove(0);
            ArrayList<Neuron> coucheCache = new ArrayList<>();
            for(String str_neuron : str_coucheCache)
                coucheCache.add(new Neuron(str_neuron));
            couchesCachees.add(coucheCache);
        }

        ArrayList<String> str_coucheSortie = new ArrayList<String>(Arrays.asList(filecontents.split("coucheSortie=")[1].split("coucheSortie=")[0].split("Neurone")));
        str_coucheSortie.remove(0);
        for(String str_neuron : str_coucheSortie)
            coucheSortie.add(new Neuron(str_neuron));
    }

    abstract public TrainingSet makeTrainingSet();

    /**
     * Permet de lancer l’apprentissage d’un réseau de neurones en se basant sur la rétro-propagation du gradient .
     */
    public void apprentissage(NetworkPanel panel,NetworkCanvas canvas,int ns) {
        ArrayList<ArrayList<Float>> entrees = training_set.getInputs();
        ArrayList<ArrayList<Float>> sortiesAttendues = training_set.getOutputs();

        int testReussis;
        int testsTotals = 0;

        //Tant que l'apprentissage n'est pas finis
        while (!estApprentissageFinis) {
            estApprentissageFinis = true;
            testReussis = 0;

            for(int i = 0; i < entrees.size(); i++)
            {
                ArrayList<Float> entree = entrees.get(i);
                ArrayList<Float> sortie = calculateWithNetwork(entree);
                ArrayList<Float> sortieAttendue = sortiesAttendues.get(i);

                testsTotals++;
                boolean isTestOk = true;
                //Test les sorties du réseau
                for(int j = 0; j < sortie.size(); j++)
                {
                    Float pourcentageSortie = sortie.get(j);
                    Float pourcentageSortieAttendue = sortieAttendue.get(j);

                    //Erreur d'apprentissage
                    if(pourcentageSortie.isNaN() || pourcentageSortie.isInfinite())
                        return;

                    //Si la marge d'erreur est trop grande
                    if(Math.abs(pourcentageSortie-pourcentageSortieAttendue) > toleranceSortie)
                    {
                        //Test échoué et apprentissage pas finis
                        isTestOk = false;
                        estApprentissageFinis = false;

                        try {
                            Thread.sleep(0,ns);
                        }
                        catch (Exception e){}
                        if(panel != null) {
                            panel.setNetwork(this);
                            panel.revalidate();
                            panel.repaint();
                        }
                        if(canvas != null){
                            canvas.redraw();
                        }

                        /** Début apprentissage / Rétro-propagation de l'erreur **/
                        //Signal d’erreur (globale) da la couche de sortie
                        ArrayList<Float> erreurGlobale = new ArrayList<Float>();
                        for(int k=0;k<coucheSortie.size();k++)
                        {
                            //Sorties désirées - Sorties obtenues
                            erreurGlobale.add(sortieAttendue.get(k) - coucheSortie.get(k).getSortie());
                        }

                        //Calcul du gradient local (signal d’erreur local) de la couche de sortie
                        ArrayList<Float> erreurLocalSortie = new ArrayList<Float>();
                        for(int k=0;k<coucheSortie.size();k++)
                        {
                            erreurLocalSortie.add(erreurGlobale.get(k)*coucheSortie.get(k).getSortie()*(1f-coucheSortie.get(k).getSortie()));
                        }

                        //Parcourt les neurones de la couche de sortie et modifie les poids
                        for(int k=0;k<coucheSortie.size();k++)
                        {
                            Neuron neuron = coucheSortie.get(k);
                            for(int l = 0; l< neuron.getPoids().length; l++)
                                neuron.getPoids()[l] = neuron.getPoids()[l] + eta*erreurLocalSortie.get(k)* couchesCachees.get(couchesCachees.size()-1).get(l).getSortie();
                        }

                        ArrayList<Float> erreurLocalSuivante = erreurLocalSortie;
                        ArrayList<Neuron> coucheSuivante = coucheSortie;
                        for(int m=couchesCachees.size()-1; m>=0;m--)
                        {
                            ArrayList<Neuron> coucheCachee = couchesCachees.get(m);
                            ArrayList<Neuron> couchePrecedente = (m==0)?null:couchesCachees.get(m-1);

                            //Calcul du gradient local (signal d’erreur local) de la couche cachee
                            ArrayList<Float> erreurLocalCachee = new ArrayList<Float>();
                            for(int k=0;k<coucheCachee.size();k++)
                            {
                                Neuron neuron = coucheCachee.get(k);
                                Float erreurDuNeurone = 0f;

                                for(int l=0;l<coucheSuivante.size();l++)
                                {
                                    Neuron neuronCible = coucheSuivante.get(l);
                                    erreurDuNeurone +=
                                            neuron.getSortie()
                                            *(1f- neuron.getSortie())
                                            * neuronCible.getPoids()[k]*
                                                    erreurLocalSuivante.get(l);
                                }
                                erreurLocalCachee.add(erreurDuNeurone);
                            }

                            //Parcourt les neurones de la couche cachee et modifie les poids
                            for(int k=0;k<coucheCachee.size();k++)
                            {
                                Neuron neuron = coucheCachee.get(k);
                                for(int l = 0; l< neuron.getPoids().length; l++) {
                                    Float output = (couchePrecedente != null)?couchePrecedente.get(l).getSortie():entree.get(l);
                                    neuron.getPoids()[l] = neuron.getPoids()[l] + eta * erreurLocalCachee.get(k) * output;
                                }
                            }
                            erreurLocalSuivante = erreurLocalCachee;
                            coucheSuivante = coucheCachee;
                        }
                        break;
                    }
                }
                if(isTestOk)
                    testReussis++;
            }

            System.out.println(testReussis+"/"+entrees.size());
        }
        System.out.println(testsTotals+" tests fait");
    }

    public void apprentissage() {
        apprentissage(null,null,0);
    }
    public void apprentissage(NetworkCanvas canvas, int ns) {
        apprentissage(null,canvas,ns);
    }
    public void apprentissage(NetworkPanel panel, int ns) {
        apprentissage(panel,null,ns);
    }

    //Méthode pour permettre à l'utilisateur d'utiliser le réseau
    public ArrayList<Float> calculateWithNetwork(ArrayList<Float> entree)
    {
        //Tableau 2D stockant les sorties des couches
        ArrayList<ArrayList<Float>> sortiesCouchesCachees = new ArrayList<ArrayList<Float>>();
        //La première sortie est la liste d'entrées
        sortiesCouchesCachees.add(entree);

        //Pour chaque couche cachées
        for(ArrayList<Neuron> coucheCachee : couchesCachees)
        {
            //Définis sa liste de sorties
            ArrayList<Float> sortieCoucheCache = new ArrayList<Float>();
            //Pour chaque neurone de cette couche
            for(Neuron neuron : coucheCachee)
            {
                //Calcule la sortie du neurone pour la dernière couche du tableau 2D
                neuron.metAJour(sortiesCouchesCachees.get(sortiesCouchesCachees.size()-1));
                sortieCoucheCache.add(neuron.getSortie());
            }
            //Les sorties calculées deviennent la dernière couche
            sortiesCouchesCachees.add(sortieCoucheCache);
        }


        ArrayList<Float> sortiesCoucheSortie = new ArrayList<Float>();
        for(Neuron neuron : coucheSortie)
        {
            neuron.metAJour(sortiesCouchesCachees.get(sortiesCouchesCachees.size()-1));
            sortiesCoucheSortie.add(neuron.getSortie());
        }

        return sortiesCoucheSortie;
    }

    @Override
    public String toString() {
        return "Network{" +
                "eta=" + eta +
                ", toleranceSortie=" + toleranceSortie +
                ", estApprentissageFinis=" + estApprentissageFinis +
                ", couchesCachees=" + couchesCachees +
                ", coucheSortie=" + coucheSortie +
                '}';
    }

    /**
     * fonction de sauvegarde d'un réseau de neurones (le fichier se retrouvera dans le répertoire savedNeuralNetworks du dossier ressources)
     * @param filename nom du fichier a enregistrer
     */
    public void save(String filename){
        try {
            File file = new File("./src/main/resources/SavedNeuralNetworks/" + filename + ".txt");
            if (file.createNewFile()) {
                System.out.println("File created: " + file.getName());
            } else {
                System.out.println("File already exists.");
            }
            FileWriter myWriter = new FileWriter(file.getAbsoluteFile());
            myWriter.write(this.toString());
            myWriter.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /** GETTERS / SETTERS **/

    public int getNbInput(){
        try{
            return couchesCachees.get(0).get(0).getPoids().length;
        }
        catch (NullPointerException e) {
            return 0;
        }
    }

    public float getMinWeight(){
        float min = Float.POSITIVE_INFINITY;
        for(ArrayList<Neuron> list_n : couchesCachees){
            for(Neuron n : list_n){
                for(float weight : n.getPoids()){
                    min = Math.min(min,weight);
                }
            }
        }
        for(Neuron n : coucheSortie){
            for(float weight : n.getPoids()){
                min = Math.min(min,weight);
            }
        }
        return min;
    }

    public float getMaxWeight(){
        float max = Float.NEGATIVE_INFINITY;
        for(ArrayList<Neuron> list_n : couchesCachees){
            for(Neuron n : list_n){
                for(float weight : n.getPoids()){
                    max = Math.max(max,weight);
                }
            }
        }
        for(Neuron n : coucheSortie){
            for(float weight : n.getPoids()){
                max = Math.max(max,weight);
            }
        }
        return max;
    }

    public float getEta() {
        return eta;
    }

    public float getToleranceSortie() {
        return toleranceSortie;
    }

    public ArrayList<Neuron> getCoucheSortie() {
        return coucheSortie;
    }

    public void setCoucheSortie(ArrayList<Neuron> coucheSortie) {
        this.coucheSortie = coucheSortie;
    }

    public ArrayList<ArrayList<Neuron>> getCouchesCachees() {
        return couchesCachees;
    }

    public void setEta(float eta) {
        this.eta = eta;
    }

    public void setToleranceSortie(float toleranceSortie) {
        this.toleranceSortie = toleranceSortie;
    }

    public boolean isEstApprentissageFinis() {
        return estApprentissageFinis;
    }

    public void setEstApprentissageFinis(boolean estApprentissageFinis) {
        this.estApprentissageFinis = estApprentissageFinis;
    }

    public TrainingSet getTraining_set() {
        return training_set;
    }

    public void setTraining_set(TrainingSet training_set) {
        this.training_set = training_set;
    }

    public void setCouchesCachees(ArrayList<ArrayList<Neuron>> couchesCachees) {
        this.couchesCachees = couchesCachees;
    }

    public ArrayList<String> getOutput_name() {
        return output_name;
    }

    public void setOutput_name(ArrayList<String> output_name) {
        this.output_name = output_name;
    }
}
