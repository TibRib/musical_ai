package fr.mAIstro.ai.neuralnetwork.implementations;

import fr.mAIstro.ai.neuralnetwork.NetworkSig;
import fr.mAIstro.ai.neuralnetwork.TrainingSet;
import fr.mAIstro.audio.Music;
import fr.mAIstro.lib.FileService;

import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.Arrays;

public class AI_Genre extends NetworkSig {
    enum GENRE {CLASSIQUE,ELECTRONIQUE,JAZZ,METAL,RAP,ROCK};

    public AI_Genre(){
        super(0.1f,0.2f,6,8,3,6);
        this.setOutput_name(new ArrayList<String>(Arrays.asList("Classique", "Electronique", "Jazz", "Metal","Rap", "Rock")));
    }

    public AI_Genre(String str){
        super(str);
        this.setOutput_name(new ArrayList<String>(Arrays.asList("Classique", "Electronique", "Jazz", "Metal","Rap", "Rock")));
    }

    @Override
    /**
     * Le set d'entrainement est fabriqué a partir des instruments identifiés par l'IA Instruments sur une musique et de son BPM
     */
    public TrainingSet makeTrainingSet() {
        AI_Instrument ai_instrument = new AI_Instrument("./src/main/resources/SavedNeuralNetworks/IA_instruments.txt");

        ArrayList<ArrayList<Float>> setEntrainement = new ArrayList<ArrayList<Float>>();
        ArrayList<ArrayList<Float>> setReponses = new ArrayList<ArrayList<Float>>();
        String musicfolderpath = "./samples/";
        ArrayList<String> genrelist = new ArrayList<String>(Arrays.asList("Classique", "Electronique", "Jazz", "Metal","Rap", "Rock"));

        int cptgenre = 0;
        for(String strgenre : genrelist) {
            for (String filename : FileService.getListFile(musicfolderpath + strgenre + "/", "wav")) {
                Music music = new Music(musicfolderpath + strgenre + "/" + filename);
                ArrayList<Float> inputs = ai_instrument.solve(music);
                inputs.add((float)music.getBPM());
                setEntrainement.add(inputs);
                setReponses.add(new ArrayList<Float>(Arrays.asList(
                        cptgenre == 0 ? 1f : 0f,
                        cptgenre == 1 ? 1f : 0f,
                        cptgenre == 2 ? 1f : 0f,
                        cptgenre == 3 ? 1f : 0f,
                        cptgenre == 4 ? 1f : 0f,
                        cptgenre == 5 ? 1f : 0f,
                        cptgenre == 6 ? 1f : 0f
                )));
            }
            cptgenre++;
        }
        return new TrainingSet(setEntrainement,setReponses);
    }

    public ArrayList<Float> solve(ArrayList<Float> probaInstru,Music music){
        ArrayList<Float> in = probaInstru;
        in.add((float)music.getBPM());
        return this.calculateWithNetwork(in);
    }
}
