package fr.mAIstro.ai.neuralnetwork.implementations;

import fr.mAIstro.ai.neuralnetwork.NetworkSig;
import fr.mAIstro.ai.neuralnetwork.TrainingSet;

import java.util.ArrayList;
import java.util.Arrays;

/**
 * IA de test pour voir si le systeme de reseau de neurones sigmoide fonctione
 */
public class AI_Shmilblick extends NetworkSig {
    //Cst par défaut
    public AI_Shmilblick() {
        super(0.01f, 0.15f,3,3, 2, 5);
        this.setOutput_name(new ArrayList<String>(Arrays.asList("22", "4", "-7", "3", "8")));
    }

    //Charge depuis un fichier
    public AI_Shmilblick(String filename) {
        super(filename);
        this.setOutput_name(new ArrayList<String>(Arrays.asList("22", "4", "-7", "3", "8")));
    }

    @Override
    public TrainingSet makeTrainingSet(){
        ArrayList<ArrayList<Float>> setEntrainement = new ArrayList<ArrayList<Float>>();

        setEntrainement.add(new ArrayList<Float>(Arrays.asList(0f,0f,0f)));
        setEntrainement.add(new ArrayList<Float>(Arrays.asList(0f,0f,1f)));
        setEntrainement.add(new ArrayList<Float>(Arrays.asList(0f,1f,0f)));
        setEntrainement.add(new ArrayList<Float>(Arrays.asList(0f,1f,1f)));
        setEntrainement.add(new ArrayList<Float>(Arrays.asList(1f,0f,0f)));
        setEntrainement.add(new ArrayList<Float>(Arrays.asList(1f,0f,1f)));
        setEntrainement.add(new ArrayList<Float>(Arrays.asList(1f,1f,0f)));
        setEntrainement.add(new ArrayList<Float>(Arrays.asList(1f,1f,1f)));

        ArrayList<ArrayList<Float>> setReponses = new ArrayList<ArrayList<Float>>();
        setReponses.add(new ArrayList<Float>(Arrays.asList(1f, 0f, 0f, 0f, 0f))); //22
        setReponses.add(new ArrayList<Float>(Arrays.asList(0f, 1f, 0f, 0f, 0f))); //4
        setReponses.add(new ArrayList<Float>(Arrays.asList(0f, 0f, 1f, 0f, 0f))); //4
        setReponses.add(new ArrayList<Float>(Arrays.asList(0f, 0f, 0f, 1f, 0f))); //-7
        setReponses.add(new ArrayList<Float>(Arrays.asList(0f, 0f, 0f, 0f, 1f))); // 4
        setReponses.add(new ArrayList<Float>(Arrays.asList(0f, 0f, 0f, 1f, 1f))); // 3
        setReponses.add(new ArrayList<Float>(Arrays.asList(0f, 0f, 1f, 1f, 1f))); //-7
        setReponses.add(new ArrayList<Float>(Arrays.asList(1f, 0f, 0f, 0f, 0f))); //8

        TrainingSet t = new TrainingSet(setEntrainement,setReponses);
        return t;
    }
}
