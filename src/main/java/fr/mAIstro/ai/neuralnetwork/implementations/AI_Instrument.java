package fr.mAIstro.ai.neuralnetwork.implementations;

import fr.mAIstro.ai.neuralnetwork.NetworkSig;
import fr.mAIstro.ai.neuralnetwork.TrainingSet;
import fr.mAIstro.audio.AudioSample;
import fr.mAIstro.audio.Music;
import fr.mAIstro.lib.FileService;

import java.util.ArrayList;
import java.util.Arrays;

public class AI_Instrument extends NetworkSig {
    enum GENRE {CLASSIQUE,ELECTRONIQUE,JAZZ,METAL,POP,RAP,ROCK};

    //Cst par défaut
    public AI_Instrument(){
        super(0.15f,0.1f,5,25,3,5);
        this.setOutput_name(new ArrayList<String>(Arrays.asList("Piano", "Guitare", "Saxophone", "Violon", "Flute")));
    }

    //Load par fichier
    public AI_Instrument(String path){
        super(path);
        this.setOutput_name(new ArrayList<String>(Arrays.asList("Piano", "Guitare", "Saxophone", "Violon", "Flute")));
    }

    @Override
    //Fabrique un set d'entrainement spécifique pour IA instruments
    public TrainingSet makeTrainingSet() {
        ArrayList<ArrayList<Float>> setEntrainement = new ArrayList<ArrayList<Float>>();
        ArrayList<ArrayList<Float>> setReponses = new ArrayList<ArrayList<Float>>();

        //Fichier racine des instruments
        String musicfolderpath = "./src/main/resources/music/instruments/";
        ArrayList<String> instrumentslist = new ArrayList<>();

        //Liste des différents dossier d'instruments
        instrumentslist.add("piano");
        instrumentslist.add("saxophone");
        instrumentslist.add("guitar");
        instrumentslist.add("violon");
        instrumentslist.add("flute");

        int cptInstrument = 0;
        //Pour chaque instruments
        for(String strInstrument : instrumentslist) {

            //Pour chaque fichier musical
            for (String filename : FileService.getListFile(musicfolderpath + strInstrument + "/", "wav")) {
                //Load le fichier
                Music music = new Music(musicfolderpath + strInstrument + "/" + filename);
                //Cablage les informations
                music.associeNotes();
                //Pour chaque échantillon
                for(AudioSample sample : music.getSamples())
                {
                    //Si l'échantillon est valable
                    if(!sample.isSilent() && sample.getFrequenceObtenue() != 0 && sample.getHarmoniques().length == 5) {
                        ArrayList<Float> inputs = new ArrayList<>();

                        //Ajoute les harmoniques en entrées
                        for (Float f : sample.getHarmoniques()) {
                            inputs.add(f);
                        }
                        //Définie les sorties attendues
                        setEntrainement.add(inputs);
                        setReponses.add(new ArrayList<Float>(Arrays.asList(
                                cptInstrument == 0 ? 1f : 0f, //piano
                                cptInstrument == 1 ? 1f : 0f, //saxo
                                cptInstrument == 2 ? 1f : 0f, //guitar
                                cptInstrument == 3 ? 1f : 0f, //violon
                                cptInstrument == 4 ? 1f : 0f //flute
                        )));
                    }
                }
            }
            //Passe à l'instrument suivant
            cptInstrument++;
        }
        return new TrainingSet(setEntrainement,setReponses);
    }

    //Méthode pour utiliser l'IA instrument depuis l'extérieur
    public ArrayList<Float> solve(Music music){
        ArrayList<Float> out = new ArrayList<>();
        float pPiano = 0;
        float pGuitar = 0;
        float pSax = 0;
        float pViolon = 0;
        float pFlute = 0;
        float pTot = 0;

        AI_Instrument ai = new AI_Instrument("./src/main/resources/SavedNeuralNetworks/IA_instruments.txt");
        Music m = new Music("./src/main/resources/music/128_D#m_DreamySub_01_726.wav");
        m.associeNotes();
        for(AudioSample sample : m.getSamples()) {
            if(!sample.isSilent() && sample.getFrequenceObtenue() != 0) {
                ArrayList<Float> inputs = new ArrayList<>();
                for(float f : sample.getHarmoniques())
                    inputs.add(f);
                ArrayList<Float> answers = ai.calculateWithNetwork(inputs);
                pPiano += answers.get(0);
                pGuitar += answers.get(1);
                pSax += answers.get(2);
                pViolon += answers.get(3);
                pFlute += answers.get(4);
                pTot++;
            }
        }
        out.add(pPiano/pTot);
        out.add(pGuitar/pTot);
        out.add(pSax/pTot);
        out.add(pViolon/pTot);
        out.add(pFlute/pTot);
        return out;
    }
}
