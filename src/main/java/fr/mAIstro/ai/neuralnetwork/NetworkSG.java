package fr.mAIstro.ai.neuralnetwork;

public abstract class NetworkSG extends Network{

    public NetworkSG(float eta, float toleranceSortie, int nb_entree, int nbNeuronesCoucheEntree, int nbCouchesCachees, int nb_sortie) {
        super(eta, toleranceSortie, nb_entree, nbNeuronesCoucheEntree, nbCouchesCachees, nb_sortie);
    }

    public NetworkSG(String filename) {
        super(filename);
    }
}
