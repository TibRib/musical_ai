package fr.mAIstro.ai.neuralnetwork;

import fr.mAIstro.lib.FileService;

import java.io.File;
import java.io.FileWriter;
import java.util.ArrayList;
import java.util.Arrays;

/**
 * Set d'entrainement d'un réseau de neurone, pouvant etre chargé a partir d'un fichier
 */
public class TrainingSet {
    ArrayList<ArrayList<Float>> inputs,outputs;

    /**
     * création d'un TrainingSet en partant de valeurs d'entrée et de sorties connues
     * @param inputs entrées
     * @param outputs sorties
     */
    public TrainingSet(ArrayList<ArrayList<Float>> inputs, ArrayList<ArrayList<Float>> outputs) {
        this.inputs = inputs;
        this.outputs = outputs;
    }

    /**
     * Création d'un set d'entrainement a partir d'un fichier provenant d'un set pré-enregistré
     * @param filename chemin vers le fichier
     */
    public TrainingSet(String filename){
        inputs = new ArrayList<>();
        outputs = new ArrayList<>();
        String filecontents = FileService.getFileContents(filename);

        ArrayList<String> str_list_inputs = new ArrayList<String>(Arrays.asList(
                filecontents.split("inputs=\\[")[1].
                        split("outputs=")[0].
                        split("], \\[")
        ));
        for(String str_no_split : str_list_inputs){
            ArrayList<String> str_inputs = new ArrayList<>(Arrays.asList(
                    str_no_split.replace("]], ","")
                            .replace("[","")
                            .replace("]","")
                            .split(","))
            );
            ArrayList<Float> floats = new ArrayList<>();
            for(String str : str_inputs)
                floats.add(Float.parseFloat(str));
            inputs.add(floats);
        }

        ArrayList<String> str_list_outputs = new ArrayList<String>(Arrays.asList(
                filecontents.split("outputs=\\[")[1].
                        split("]}")[0].
                        split("], \\[")
        ));
        for(String str_no_split : str_list_outputs){
            ArrayList<String> str_inputs = new ArrayList<>(Arrays.asList(
                    str_no_split.replace("]], ","")
                            .replace("[","")
                            .replace("]","")
                            .split(","))
            );
            ArrayList<Float> floats = new ArrayList<>();
            for(String str : str_inputs)
                floats.add(Float.parseFloat(str));
            outputs.add(floats);
        }
    }

    public ArrayList<ArrayList<Float>> getInputs() {
        return inputs;
    }

    public void setInputs(ArrayList<ArrayList<Float>> inputs) {
        this.inputs = inputs;
    }

    public ArrayList<ArrayList<Float>> getOutputs() {
        return outputs;
    }

    public void setOutputs(ArrayList<ArrayList<Float>> outputs) {
        this.outputs = outputs;
    }

    @Override
    public String toString() {
        return "TrainingSet{" +
                "inputs=" + inputs +
                ", outputs=" + outputs +
                '}';
    }

    /**
     * fonction de sauvegarde d'un set d'entrainement (le fichier se retrouvera dans le répertoire savedNeuralNetworks du dossier ressources)
     * @param filename nom du fichier a enregistrer
     */
    public void save(String filename){
        try {
            File file = new File("./src/main/resources/SavedTrainingSets/" + filename + ".txt");
            if (file.createNewFile()) {
                System.out.println("File created: " + file.getName());
            } else {
                System.out.println("File already exists.");
            }
            FileWriter myWriter = new FileWriter(file.getAbsoluteFile());
            myWriter.write(this.toString());
            myWriter.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
