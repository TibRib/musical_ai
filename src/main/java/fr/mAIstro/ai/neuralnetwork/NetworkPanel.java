package fr.mAIstro.ai.neuralnetwork;
import javax.swing.*;
import java.awt.*;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;

/**
 * Panneau de dessin Swing représentant un réseau de neurone graphiquement
 */
public class NetworkPanel extends JPanel {
    //réseau de neurone cible
    Network network;

    // Inner-class servant a effectuer des calculs sur les positions des différents éléments constituant le réseau de neurones
    private class NetworkGraphInfo{
        //largeur du panneau
        public int w;
        //hauteur du panneau
        public int h;

        //nombre de couches cachées du réseau de neurones
        public int nbcouchecache;
        //nombre total de couches de neurones
        public int nbcouches;
        //nombre d'entrées
        public int nbinput;
        //nombre de sorties
        public int nboutput;
        //nombre maximu de neurone sur une couche du réseau
        public int maxnbneurone;

        public int[] nbneuronecache;

        //position des centres des cercles représentant les neurones
        public ArrayList<ArrayList<int[]>> centers;
        //rayon des cercles
        public int r;
        //distance en longueur entre les neurones
        public int dx;

        //largeur totale de la representation graphique
        public int fullwidth;

        //poids maximum et minimum des neurones
        public float min_weight,max_weight;

        public NetworkGraphInfo(NetworkPanel pan){
            w = pan.getWidth();
            h = pan.getHeight();

            min_weight = network.getMinWeight();
            max_weight = network.getMaxWeight();

            nbcouchecache = network.getCouchesCachees().size();
            nbcouches = 2 + nbcouchecache;
            nbinput = network.getNbInput();
            nboutput = network.getCoucheSortie().size();
            maxnbneurone = Math.max(nbinput,nboutput);

            nbneuronecache = new int[nbcouchecache];
            for(int i=0;i<nbcouchecache;i++){
                nbneuronecache[i] = network.getCouchesCachees().get(i).size();
                maxnbneurone = Math.max(maxnbneurone,nbneuronecache[i]);
            }

            centers = new ArrayList<>();
            for(int i = 0;i<nbcouches;i++)
                centers.add(new ArrayList<>());

            r = w/34;
            dx = (int)(r*4f);

            fullwidth = r + (nbcouches-1)*dx ;

            for(int i=0;i<network.getNbInput();i++){
                int[] center = {w/2 - fullwidth/2 +r/2,h-(i+2)*(h/(nbinput+2))+r/2};
                centers.get(0).add(center);
            }

            for(int i=0;i<nbcouchecache;i++){
                for(int i2=0;i2<nbneuronecache[i];i2++) {
                    int[] center = {w / 2 - fullwidth / 2 + (i + 1) * dx+r/2,h - (i2 + 2) * (h / (nbneuronecache[i] + 2))+r/2};
                    centers.get(i+1).add(center);
                }
            }

            for(int i=0;i<network.getCoucheSortie().size();i++){
                int[] center = {w/2 - fullwidth/2 + (nbcouchecache+1)*dx +r/2,h-(i+2)*(h/(nboutput+2))+r/2};
                centers.get(nbcouches-1).add(center);
            }
        }
    }

    //fais apparaitre le panneau dans une JFrame
    public JFrame appearAsWindow(){
        JFrame fenetre = new JFrame();    // Fabrique la fenêtre
        fenetre.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);    // Quand on ferme la fenêtre, l'application s'arrête
        try {
            fenetre.getContentPane().add(this);    // Ajoute l'élément image au contenu de la fenêtre
            fenetre.pack();    // Fixe la taille de la fenêtre relativement à son contenu
            fenetre.setLocation(0, 0);
            fenetre.setSize(1920, 1080);
            fenetre.setVisible(true);                    // Affiche la fenêtre
        } catch (Exception e) {
            System.out.println("Impossible d'ouvrir le panneau (" + e + ")");
        }
        return fenetre;
    }

    public NetworkPanel(Network network) {
        this.network = network;
    }

    // Chaque fois que le contenu du composant doit être redessiné, cette méthode
    // est appelée : il faut donc y mettre ce que l'on veut afficher, ici : image
    protected void paintComponent(Graphics gg) {
        Graphics2D g = (Graphics2D) gg;
        super.paintComponent(g);	// Exécute les actions graphiques héritées

        NetworkGraphInfo info = new NetworkGraphInfo(this);

        Stroke stroke = new BasicStroke(5.0f, BasicStroke.CAP_BUTT, BasicStroke.JOIN_MITER);
        g.setStroke(stroke);

        g.setColor(Color.blue);
        for(int i=0;i<network.getNbInput();i++){
            int cx = info.centers.get(0).get(i)[0];
            int cy = info.centers.get(0).get(i)[1];
            g.drawOval(cx-info.r/2, cy-info.r/2,info.r,info.r);
        }

        g.setColor(Color.black);
        for(int i=0;i<info.nbcouchecache;i++){
            for(int i2=0;i2<info.nbneuronecache[i];i2++) {
                int cx = info.centers.get(i + 1).get(i2)[0];
                int cy = info.centers.get(i + 1).get(i2)[1];
                g.setStroke(stroke);
                g.drawOval(cx-info.r/2, cy-info.r/2,info.r,info.r);
                int i3 = 0;
                for(int[] point : info.centers.get(i)){
                    float borne = info.max_weight - info.min_weight;
                    float nb = network.getCouchesCachees().get(i).get(i2).getPoids()[i3] - info.min_weight;
                    g.setStroke(new BasicStroke(6*(1.f*nb/borne)+1, BasicStroke.CAP_BUTT, BasicStroke.JOIN_MITER));
                    g.drawLine(cx,cy,point[0],point[1]);
                    i3++;
                }
            }
        }

        g.setColor(Color.red);
        for(int i=0;i<network.getCoucheSortie().size();i++){
            int cx = info.centers.get(info.nbcouches-1).get(i)[0];
            int cy = info.centers.get(info.nbcouches-1).get(i)[1];
            g.setStroke(stroke);
            g.drawOval(cx-info.r/2, cy-info.r/2,info.r,info.r);
            int i3 = 0;
            for(int[] point : info.centers.get(info.nbcouches-2)){
                float borne = info.max_weight - info.min_weight;
                float nb = network.getCoucheSortie().get(i).getPoids()[i3] - info.min_weight;
                g.setStroke(new BasicStroke(6*(1.f*nb/borne)+1, BasicStroke.CAP_BUTT, BasicStroke.JOIN_MITER));
                g.drawLine(cx,cy,point[0],point[1]);
                i3++;
            }
        }
        g.drawString(LocalDateTime.now().format(DateTimeFormatter.ISO_DATE_TIME),100,100);
    }

    public Network getNetwork() {
        return network;
    }

    public void setNetwork(Network network) {
        this.network = network;
    }
}
