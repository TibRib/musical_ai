package fr.mAIstro.ai.neuron;

public class NeuronSG extends Neuron {

    public NeuronSG(int nbEntrees) {
        super(nbEntrees);
        this.setBiais(0.2f);
    }

    @Override
    public float activation(final float valeur) {
        //Fonction seuil généralisé
        return valeur >= 0 ? 1.f : 0.f;
    }
}
