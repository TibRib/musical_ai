package fr.mAIstro.ai.neuron;

import java.util.ArrayList;
import java.util.Arrays;

public class Neuron {

        private float[] poids;
        private float biais;
        private float sortie = Float.NaN;

        // Constructeur d'un neurone
        public Neuron(final int nbEntrees)
        {
            // Le tableau des poids synaptiques
            poids = new float[nbEntrees];

            biais = -1f;

            // On initialise tous les poids de manière alétoire
            for (int i = 0; i < nbEntrees; ++i) {
                poids[i] = -0.1f+(float) Math.random()*0.2f;
            }

        }

        //Load un neurone à partir d'un fichier
        public Neuron(String strToParse){
            biais = Float.parseFloat(strToParse.split("biais=")[1].split(",")[0]);
            sortie = Float.parseFloat(strToParse.split("sortie=")[1].split("}")[0]);
            String str_poids = strToParse.split("poids=\\[")[1].replaceAll(" ","").split("]")[0];
            poids = new float[str_poids.split(",").length];
            int cpt = 0;
            for(String nb : str_poids.split(",")){
                poids[cpt] = Float.parseFloat(nb);
                cpt++;
            }

        }

        // Fonction d'activation d'un neurone, Par défaut sigmoide
        public float activation(final float valeur) {
            //Fonction sigmoid
            return (float) (1f/(1f+Math.exp(-valeur)));
        }

        // Calcule la valeur de sortie en fonction des entrées, des poids synaptiques,
        // du biais et de la fonction d'activation
        public void metAJour(final ArrayList<Float> entrees)
        {
            // On démarre en extrayant le biais
            float somme = biais;

            // Puis on ajoute les produits entrée-poids synaptique
            for (int i = 0; i < poids.length; i++)
                somme += entrees.get(i).floatValue()*poids[i];

            // On fixe la sortie du neurone relativement à la fonction d'activation
            sortie = activation(somme);
        }

        /** GETTERS / SETTERS **/

        public float[] getPoids() {
            return poids;
        }

        public void setPoids(float[] poids) {
            this.poids = poids;
        }

        public float getBiais() {
            return biais;
        }

        public void setBiais(float biais) {
            this.biais = biais;
        }

        public float getSortie() {
            return sortie;
        }

        public void setSortie(float sortie) {
            this.sortie = sortie;
        }

    @Override
    public String toString() {
        return "Neurone{" +
                "poids=" + Arrays.toString(poids) +
                ", biais=" + biais +
                ", sortie=" + sortie +
                '}';
    }
}
