package fr.mAIstro.ai.neuron;

public class NeuronLin extends Neuron {

    public NeuronLin(int nbEntrees) {
        super(nbEntrees);
        this.setBiais(0.2f);
    }

    @Override
    public float activation(final float valeur) {
        //Fonction linéraire
        return valeur;
    }
}
