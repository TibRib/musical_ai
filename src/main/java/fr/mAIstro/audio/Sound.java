package fr.mAIstro.audio;

import javax.sound.sampled.*;
import java.io.File;
import java.io.IOException;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.nio.ShortBuffer;


// *****************************************************************************
// La classe Son illustre la lecture de fichiers son de type WAV,
// au format 16 bits monaural non compressé
// Installez et utilisez éventuellement Audacity dans la VM pour convertir vos
// propres sons dans ce format

public abstract class Sound {
	//fréquence de l'extrait audio
	private int frequency;
	//données du fichier wav
	private float[] data;
	//fichier loadé
	private File file;
	//longueur des données
	private int length;
	//nombre de channels
	private int nb_channels;
	//lecture arretée ?
	private boolean stopped = true;

	//echantillon de son en train d'etre lu
	double current_index = 0;

	private AudioInputStream audioStream;
	private AudioFormat audioFormat;
	//thread séparé de lecture du son
	PlayThread thread = null;

	// Constructeur d'un objet permettant de lire un fichier son mono-canal
	// 16 bits PCM little endian, en utilisant les API Java
	public Sound(final String nomFichier) {
		try {
			file = new File(nomFichier);
			// Ouvrir le fichier comme une source audio
			audioStream = AudioSystem.getAudioInputStream(file);
			// Obtenir des informations sur cette source
			audioFormat = audioStream.getFormat();

			if ( /*audioFormat.getChannels() == 1 &&	*/// Si le signal est monophonique
				audioFormat.getEncoding() == AudioFormat.Encoding.PCM_SIGNED &&	// et qu'il est en Pulse Code Modulation signé
				audioFormat.getSampleSizeInBits() == 16)	// et que les échantillons sont sur 16 bits
			{
				nb_channels = audioFormat.getChannels();
				System.out.println(audioFormat.getChannels());
				final int NombreDonnees = audioStream.available();	// Combien d'octets constituent les données
				final byte[] bufferOctets = new byte[NombreDonnees];	// Préparer un buffer pour lire tout le flux du fichier
				audioStream.read(bufferOctets);	// Lire le fichier dans le buffer d'octets
				audioStream.close();	// On peut fermer le flux du fichier
				ByteBuffer bb = ByteBuffer.wrap(bufferOctets);	// Prépare le travail sur le buffer
				bb.order(ByteOrder.LITTLE_ENDIAN);	// Indique le format des données lues dans le WAV
				ShortBuffer donneesAudio = bb.asShortBuffer();

				data = new float[donneesAudio.capacity()];
				for (int i = 0; i < data.length; ++i)
					data[i] = (float)donneesAudio.get(i);
				// Récupérer la fréquence du fichier audio
				frequency = (int)audioFormat.getSampleRate();
			}
		}
		catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * Inner-class servant a lancer la lecture de l'extrait audio sur un thread différent
	 */
	private class PlayThread extends Thread {
		public Sound s;
		public void run() {
			s.play_single_thread();
		}
	}

	/**
	 * lance la lecture multi-threadée de l'echantillon audio
	 */
	public void play(){
		if(stopped) {
			thread = new PlayThread();
			stopped = false;
			thread.s = this;
			thread.start();
		}
	}

	/**
	 * Fonction de lecture d'un son
	 */
	public void play_single_thread (){
		SourceDataLine sourceLine;
		try {
			audioStream = AudioSystem.getAudioInputStream(file);

			DataLine.Info info = new DataLine.Info(SourceDataLine.class, audioFormat);
			sourceLine = (SourceDataLine) AudioSystem.getLine(info);
			sourceLine.open(audioFormat);

			sourceLine.start();
			//conversion du buffer en bytes plutot qu'en floats
			byte[] byteBuffer = new byte[data.length];
			for ( int i=0; i<data.length; i++) {
				byteBuffer[i] = (byte)data[i];
			}
			int nBytesRead = 0;
			while (nBytesRead != -1) {
				try {
					nBytesRead = audioStream.read(byteBuffer, 0, byteBuffer.length);
				} catch (IOException e) {
					e.printStackTrace();
				}
				if (nBytesRead >= 0) {
					//lecture des bytes 4 par 4 de facon a ce qu'on puisse interrompre et reprendre la lecture a volonté
					for(;current_index+4<nBytesRead && !stopped;){
						byte[] minibuffer = new byte[4];
						minibuffer[0] = byteBuffer[(int) current_index];
						current_index++;
						minibuffer[1] = byteBuffer[(int) (current_index)];
						current_index++;
						minibuffer[2] = byteBuffer[(int) (current_index)];
						current_index++;
						minibuffer[3] = byteBuffer[(int) (current_index)];
						current_index++;
						sourceLine.write(minibuffer, 0, 4);
					}
				}
			}
			//fermeture de la sortie audio
			sourceLine.drain();
			sourceLine.close();
		}
		catch (Exception e){
			e.printStackTrace();
		}
		stopped = true;
	}

	/**
	 * arrete la lecture multi-thredée d'un extrait audio
	 */
	public void stop(){
		stopped = true;
		if(thread != null)
			thread.interrupt();
	}

	/**
	 * remet la lecture a 0
	 */
	public void resetPlayback(){
		current_index = 0;
	}

	//// GETTERS AND SETTERS ////

	public void setPlaybackPercent(float percentage){
		current_index = ((int)(percentage*data.length/100/4))*4;
	}

	public int getFrequency() {
		return frequency;
	}

	public float[] getData() {
		return data;
	}

	public float getCurrent_byte() {
		return data[(int) current_index];
	}

	public double getCurrent_index() {
		return current_index;
	}

	public void setFrequency(int frequency) {
		this.frequency = frequency;
	}

	public void setData(float[] data) {
		this.data = data;
	}
	public File getFile() {
		return file;
	}

	public int getNb_channels(){
		return nb_channels;
	}

	public void setFile(File file) {
		this.file = file;
	}

	public int getLength() {
		return length;
	}
	public void setLength(int length) {
		this.length = length;
	}
	public AudioInputStream getAudioStream() {
		return audioStream;
	}
	public void setAudioStream(AudioInputStream audioStream) {
		this.audioStream = audioStream;
	}
	public AudioFormat getAudioFormat() {
		return audioFormat;
	}
	public void setAudioFormat(AudioFormat audioFormat) {
		this.audioFormat = audioFormat;
	}
}
