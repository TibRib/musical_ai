package fr.mAIstro.audio;

import fr.mAIstro.lib.Complexe;
import fr.mAIstro.lib.ComplexeCartesien;
import fr.mAIstro.lib.FFTCplx;
import javafx.scene.Scene;
import javafx.scene.chart.LineChart;
import javafx.scene.chart.NumberAxis;
import javafx.scene.chart.XYChart;
import javafx.stage.Stage;

/*
   AudioSample : classe possédant les attributs associés au traitement de morceaux sonores
 */

public class AudioSample {
    public static int SIZE = 2048;                  //Taille d'un morceau musical - utilisé pour découper notre piste audio en un ensemble de N samples
    private static int FreqEch = 44100;             //Fréquence d'échantillonnage associée à la musique traitée - redéfinie dans le constructeur

    private static float TRESHOLD_VALUE = 0.0008f; //Valeur minimale servant à déterminer si le sample est silencieux

    private float[] echantillonsReels;         //Tableau des échantillons normalisés
    private float[] harmoniques;               //Tableau des énergies associées plus tard aux harmoniques ( ex : [2] = 0.5 -> harmonique 3 possède une puissance de 0.5)
    private float bigM = 0;                    //Le module de la fondamentale

    private Complexe[] echantillonsComplexes; //Tableau des echantillons complexes sortis du traitement FFT
    private int frequenceObtenue = -1;       //Frequence (entiere, en Hz) déterminée à partir de la fonction identifieFrequence
    private int nb_channels = 1;            //Nombre de canaux associés au son entré dans le constructeur

    private boolean isSilent = false;   //Booléen défini comme vrai si le sample est jugé trop faible pour être exploité

    /* Constructeur : préciser la fréquence d'échantillonnage, le nombre de canaux et entrer un tableau d'échantillons réels */
    public AudioSample(int freqEchantillonnage, int nb_channels, float[]  morceauReels) {
        FreqEch = freqEchantillonnage;
        this.nb_channels = nb_channels;

        //On normalise les amplitudes entrées pour mettre nos sample sur un pied d'égalité
        echantillonsReels = normalisationSample(morceauReels);

        //Application facultative du fenetrage de Hamming, pour considérer les valeurs centrales comme ayant plus d'importance
        // - utile pour réduire les phénomènes tels que l'attaque d'un instrument //
        //appliqueFenetrageHamming();

        isSilent = checkEmptiness(); //On vérifie que les amplitudes normalisées soient au dela du seul de non exploitabilité
        if(! isSilent) { //On effectue le traitement uniquement si l'échantillon n'est pas silencieux, pour optimiser
            harmoniques = new float[5];
            appliqueFFT();
        }
    }

    /* Verifie que l'amplitude des echantillons soit au dessus du seuil de "silence" */
    private boolean checkEmptiness(){
        for (int i = 0; i < echantillonsReels.length; i++) {
            if(Math.abs(echantillonsReels[i]) > TRESHOLD_VALUE){
                return false;
            }
        }
        return true;
    }

    /* Conversion de nos valeurs réelles en un tableau de complexes cartésiens à partie imaginaire nulle */
    ComplexeCartesien[] conversionReelsComplexes(float[] echReels){
        ComplexeCartesien[] ech = new ComplexeCartesien[echReels.length];
        for (int i = 0; i < echReels.length; i++) {
            ech[i] = new ComplexeCartesien(echReels[i], 0);
        }
        return ech;
    }

    /* renvoie un tableau de réels flottant, correspondant à la normalisation sur une echelle 32768
        Echelle correspondant à la valeur maximale qu'un sample puisse prendre, sur une base de Short.
     */
    private float[] normalisationSample(float[] sample){
        float[] sampleNormalises = new float[sample.length];
        for (int i = 0; i < sampleNormalises.length ; i++) {
            sampleNormalises[i] = sample[i] / 0x8000; //0x8000 = 32768 = Short.MAXVALUE+1
        }
        return sampleNormalises;
    }

    //Remplace une serie au sens JavaFX (graphes)
    public void setSerieGraphique( XYChart.Series series){
        series.getData().clear(); //On efface le contenu de la série
        for (int i = 0; i < echantillonsComplexes.length; i++) {
                series.getData().add(new XYChart.Data(i, echantillonsComplexes[i].mod()));
        }
    }

    /* Obsolete : créée et trace une fenêtre graphique representant les fréquences FFT */
    public void traceGraphique(){
        if(echantillonsComplexes !=null) {
            Stage stage = new Stage();
            stage.setTitle("Line Chart Sample");
            //Définition des axes
            final NumberAxis xAxis = new NumberAxis();
            final NumberAxis yAxis = new NumberAxis();
            xAxis.setLabel("Frequences et amplitudes");
            //Création du graphique
            final LineChart<Number,Number> lineChart =
                    new LineChart<Number,Number>(xAxis,yAxis);

            lineChart.setTitle("Fréquences de l'echantillon analysé");
            //Instanciation
            XYChart.Series energies = new XYChart.Series();
            setSerieGraphique(energies);

            //Instanciation du contenu de la fenetre
            Scene scene  = new Scene(lineChart,800,600);
            lineChart.getData().addAll(energies); //Ajout graphique
            lineChart.setCreateSymbols(false); //On ne veut pas de points

            stage.setScene(scene);
            stage.show(); //Affichage
        }
    }

    //Appelle publiquement la normalisation du sample
    public void normaliseSample(){
        echantillonsReels = normalisationSample(echantillonsReels);
    }

    /* Retourne le nombre entier d'AudioSamples necessaires pour approximer l'ensemble d'une piste.
        On passe en parametre le nombre d'échantillons que possède la piste (music.getSamples().length)
     */
    public static int evalueQuantiteEchantillonsNecessaire(int tailleAudio){
        return tailleAudio/SIZE;
    }

    //Retourne la durée, en secondes, associée à un sample
    public float dureeSecondes(){
        return (float)FreqEch/SIZE;
    }

    //Retourne la durée, en millisecondes, associée à un sample
    public int dureeMillisecondes(){
        return (int)dureeSecondes()*1000;
    }

    //Renvoie la fréquence associée à notre sample
    /* Se base sur une approche de détection d'un pic de fréquence (maxima)
        Pour en relever l'indexe, correspondant à la fréquence fondamentale
     */
    private int identifieFrequence(){
        double modulemax = Double.MIN_VALUE;    //Buffer du module maximal - défini à une valeur très basse
        int moduleMaxIndex = -1;                //Buffer de l'index dont le module est maximal

        int f[] = new int[6];                   //Ici on stocke les fréquences f0,f1,f2... avec f0 etant la fondamentale et les f suivants les harmoniques d'ordre n
        int indexHarmoniques[] = new int[6];    //Ici on stocke les index sur le sample, associés au fréquences f0,f1,f2...

        /* Parcours de 0 jusqu'à frequence de nyquist
        * La fft etant mirorée entre 0 et nbEch/2 et nbEch/2 à nbEch,
        * Il nous est possible de faire abstraction de la moitiée des données à traiter */
        for (int i = 0; i < echantillonsComplexes.length/2; i++) { //Pour chaque echantillon sorti de la FFT
            double module = echantillonsComplexes[i].mod(); //On recupere le module associé à l'échantillon
            if( module > modulemax){    //Si ce module est au dessus du maximal
                moduleMaxIndex = i;        //On redefinis l'index et la valeur du maxima
                modulemax = module;
            }
        } //Fin de l'analyse des modules - on a identifié le maximum
        bigM = (float) modulemax;   //On en profite pour sauver le module maximal - celui qui est associé à la fondamentale

        //Affectation de la fréquence fondamentale : on peut déduire sa valeur à partir de l'index correspondant au pic d'énergie (module)
        f[0] = (moduleMaxIndex) * nb_channels * FreqEch/SIZE; // On multiplie l'index du module maximal par un coefficient
                                                              // de proportionnalité permettant d'étalonner l'index sur l'ensemble de la piste,
                                                              // et ainsi de retrouver la fréquence dont le module est maximal

        //Pour chacune des harmoniques traitées
        for (int i=1; i< f.length; i++){
            f[i] = (i+1)*f[0];  //On affecte l'harmonique i comme multiple de la fondamentale (ex : harmonique 1 = 2* f0)
            indexHarmoniques[i] = f[i] / (nb_channels * FreqEch/SIZE);  //On retrouve l'index grace à un produit en croix
            float mod = 0f;       //On va récuperer le module associé à l'index fréquentiel de l'harmonique i.
            if(indexHarmoniques[i] < SIZE) { //Si, biensur, la fréquence est existante parmis nos échantillons
                mod = (float) echantillonsComplexes[indexHarmoniques[i]].mod(); //affectation de l'energie
                //System.out.println("Module de l'harmonique " + i + " = " +mod);
            }
            harmoniques[i-1] = mod; //Sauvegarde
        }

        //On la valeur correspondant à la fréquence fondamental,
        //A condition que l'index du maxima soit un index existant
        return  moduleMaxIndex>= 0 ? f[0] : 0;
    }


    private Complexe[] resultatFFT(ComplexeCartesien[] entree){
        return FFTCplx.appliqueSur(entree);
    }

    /* Application du fenetrage de hamming sur nos echantillons reels */
    /* Fenetrage cosinus - source : https://fr.mathworks.com/help/signal/ug/generalized-cosine-windows.html */
    public void appliqueFenetrageHamming(){
        for (int i = 0; i < echantillonsReels.length; i++) {
            float HammingI = (float) (0.54 - 0.46 * Math.cos( 2.0*Math.PI * i / echantillonsReels.length-1));
            echantillonsReels[i] = echantillonsReels[i] * HammingI;
        }
    }

    /* Fonction appliquant la FFT et la recherche des fréquences associées*/
    public void appliqueFFT(){
        echantillonsComplexes = resultatFFT(conversionReelsComplexes(echantillonsReels)); //Appel de la fft sur les echantillons
        frequenceObtenue = identifieFrequence(); //appel vers identification de fréquence
        /* frequences inaudibles ? on définis le sample comme silencieux */
        if(frequenceObtenue > 20000)
            isSilent = true;
    }

    //Renvoie une note - voir classe note
    // Renvoie la note associée à la fréquence identifiée
    public Note identifieNote(){
        if(frequenceObtenue == -1){
            if(echantillonsComplexes == null){
                appliqueFFT();
            }
            frequenceObtenue = identifieFrequence();
        }
        Note n = new Note(frequenceObtenue);
        return n;
    }

    public static int getSIZE() {
        return SIZE;
    }

    public Complexe[] getEchantillons() {
        return echantillonsComplexes;
    }

    public static int getFreqEch() {
        return FreqEch;
    }

    public static double getPeriode() {
        return 1.0/FreqEch;
    }

    public static int getPeriodeMs() {
        return (int) (1000.0/FreqEch);
    }

    public static void setFreqEch(int freqEch) {
        FreqEch = freqEch;
    }

    public float[] getEchantillonsReels() {
        return echantillonsReels;
    }

    public float[] getEchantillonsReelsNormalises() {
        return normalisationSample(echantillonsReels);
    }

    public void setEchantillonsReels(float[] echantillonsReels) {
        this.echantillonsReels = echantillonsReels;
    }

    public Complexe[] getEchantillonsComplexes() {
        return echantillonsComplexes;
    }

    public void setEchantillonsComplexes(Complexe[] echantillonsComplexes) {
        this.echantillonsComplexes = echantillonsComplexes;
    }

    public int getFrequenceObtenue() {
        return frequenceObtenue;
    }

    public boolean isSilent() {
        return isSilent;
    }

    public float[] getHarmoniques() {
        return harmoniques;
    }

    public float getModFondamentale(){
        return bigM;
    }

    public void setSilent(boolean silent) {
        isSilent = silent;
    }
}
