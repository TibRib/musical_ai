package fr.mAIstro.audio;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class Music extends Sound{
    private MusicalGenre genre = null;
    private String author = null;
    private ArrayList<String> instruments;
    //La liste des notes associées aux echantillons
    private List<Note> notes;
    //La liste des notes obtenues (uniques)
    private List<Note> notesObtenues;
    private int bpm;

    private float percent_saxophone = 0;
    private float percent_drums     = 0;
    private float percent_voice     = 0;
    private float percent_guitar    = 0;
    private float percent_flute     = 0;
    private float percent_piano     = 0;


    enum IndexNote{
        DO, DO_BIS, RE, RE_BIS, MI, FA, FA_BIS, SOL, SOL_BIS, LA, lA_BIS, SI
    }

    //Tableau relevant les pourcentages d'apparition de chaque note
    float[] tabFrequencesNotes = new float[12];
    float[] tabAvgOctaveNotes = new float[12];

    private AudioSample[] samples;

    public Music(String nomFichier) {
        super(nomFichier);
        for (int i = 0; i < 12; i++) {
            tabFrequencesNotes[i] = 0f;
            tabAvgOctaveNotes[i] = 0;
        }
    }
    // Renvoit le BPM selon un tableau de données(Méthode de corrélation par décalage)
    public int getBPM(){
        int BPMmin = 60;
        int BPMmax = 200;

        int decalage=0;
        float correlation=0;
        float temp=0;
        int BPM=0;
        float duree=0;
        for(int i=BPMmin;i<=BPMmax;i++){
            decalage=(4*60*this.getFrequency())/i;
            if(getData() == null)
                return 0;
            duree=this.getData().length-decalage;
            for(int j=0;j<duree;j+=this.getNb_channels()) {
                correlation +=Math.abs((this.getData()[j]*this.getData()[j+decalage])/duree);
            }
            if(temp<correlation){
                temp=correlation;
                BPM=i;
            }
            correlation=0;
        }
        return BPM;
    }

    public ArrayList<Float> getDataAI_Genre(){
        ArrayList<Float> inputs = new ArrayList<>();
        inputs.add(1f/*AMPLITUDE MOYENNE*/);
        inputs.add((float)this.getBPM());
        inputs.add(percent_flute);
        inputs.add(percent_saxophone);
        inputs.add(percent_voice);
        inputs.add(percent_drums);
        inputs.add(percent_guitar);
        inputs.add(percent_piano);
        return inputs;
    }

    public MusicalGenre getGenre() {
        return genre;
    }
    public void setGenre(MusicalGenre genre) {
        this.genre = genre;
    }
    public String getAuthor() {
        return author;
    }
    public void setAuthor(String author) {
        this.author = author;
    }
    public ArrayList<String> getInstruments() {
        return instruments;
    }
    public void setInstruments(ArrayList<String> instruments) {
        this.instruments = instruments;
    }

    private void genereEchantillonsComplexes(int sampleWidth){

        AudioSample.SIZE = sampleWidth;

        System.out.println("Génération des échantillons...");
        /* Evaluation des echantillons */
        int dataLength = this.getData().length;
        int nbSamples = AudioSample.evalueQuantiteEchantillonsNecessaire(dataLength);
        final int freqEch = this.getFrequency();

        samples = new AudioSample[nbSamples];

        /*Application d'une transformée de Fourrier */
        for (int i = 0; i < nbSamples; i++) {
            float[] morceau = Arrays.copyOfRange(this.getData(), i* AudioSample.getSIZE(), (i+1)* AudioSample.getSIZE());
            samples[i] =  new AudioSample(freqEch, this.getNb_channels() ,morceau);
        }
        System.out.println(nbSamples + " echantillons générés et analysés");
    }


    public void traceFrequences(int idSample){
        samples[idSample].traceGraphique();
    }

    public void associeNotes(int sampleWidth){
        genereEchantillonsComplexes(sampleWidth);

        this.notes = new ArrayList<Note>();
        this.notesObtenues = new ArrayList<Note>();
        for(AudioSample sample : samples){
            if(sample != null && sample.isSilent() == false) {
                Note n = sample.identifieNote();
                notes.add(n);
                boolean exists = false;
                for (int i = 0; i < notesObtenues.size(); i++) {
                    if(notesObtenues.get(i).equals(n)){
                        exists = true;
                        break;
                    }
                }
                if(exists == false){
                    notesObtenues.add(n);
                }
            }
        }
        for (Note n: notes ) {
            switch (n.getName()){
                case "do" :  tabFrequencesNotes[0]++; tabAvgOctaveNotes[0]+= n.getOctave(); break;
                case "do#" : tabFrequencesNotes[1]++; tabAvgOctaveNotes[1]+= n.getOctave(); break;
                case "ré" :  tabFrequencesNotes[2]++; tabAvgOctaveNotes[2]+= n.getOctave(); break;
                case "ré#" : tabFrequencesNotes[3]++; tabAvgOctaveNotes[3]+= n.getOctave(); break;
                case "mi" :  tabFrequencesNotes[4]++; tabAvgOctaveNotes[4]+= n.getOctave(); break;
                case "fa" :  tabFrequencesNotes[5]++; tabAvgOctaveNotes[5]+= n.getOctave(); break;
                case "fa#" : tabFrequencesNotes[6]++; tabAvgOctaveNotes[6]+= n.getOctave();break;
                case "sol" : tabFrequencesNotes[7]++; tabAvgOctaveNotes[7]+= n.getOctave();break;
                case "sol#": tabFrequencesNotes[8]++; tabAvgOctaveNotes[8]+= n.getOctave();break;
                case "la" :  tabFrequencesNotes[9]++; tabAvgOctaveNotes[9]+= n.getOctave();break;
                case "la#":  tabFrequencesNotes[10]++; tabAvgOctaveNotes[10]+= n.getOctave();break;
                case "si" :  tabFrequencesNotes[11]++; tabAvgOctaveNotes[11]+= n.getOctave();break;
            }
        }
        for (int i = 0; i < 12; i++) {
            tabAvgOctaveNotes[i] = (float)tabAvgOctaveNotes[i] / (float)tabFrequencesNotes[i];
            tabFrequencesNotes[i] = tabFrequencesNotes[i]/notes.size();
            System.out.println("["+i+"] Octave moyenne = "+ tabAvgOctaveNotes[i]);
            System.out.println("["+i+"] Pourcentage d'apparition de la note "+ tabFrequencesNotes[i]);
        }


        //VERIFICATION DES AMPLITUDES MOD TROP BASSES
        float modmax = 0;
        for (AudioSample sample: samples ) {
            if (sample.getModFondamentale() > modmax){
                modmax = sample.getModFondamentale();
            }
            if (sample.getModFondamentale() < modmax*0.3){
                sample.setSilent(true);
            }
        }
    }

    public void associeNotes(){
        this.associeNotes(AudioSample.getSIZE());
    }

    public List<Note> getNotes(){
        return this.notes;
    }

    public void displayNotesSamples(){
        System.out.println("Notes samples : ");
        for ( Note note: notes ) {
            System.out.println(note.toString());
        }
    }

    public void displayNotes(){
        System.out.println("Notes relevées : ");
        for ( Note note: notesObtenues ) {
            System.out.println(note.toString());
        }
    }

    public void playNotes(List<Note> liste){
        for ( Note note: liste ) {
            note.play(44100, 120);
        }
    }

    public void playNotes(int bpm){
        for ( Note note: notes ) {
            note.play(44100, bpm);
        }
    }
    public void playNotes(){
        this.playNotes(120);
    }

    public int getBpm() {
        return bpm;
    }

    public float[] getTabFrequencesNotes() {
        return tabFrequencesNotes;
    }

    public float[] getTabAvgOctaveNotes() {
        return tabAvgOctaveNotes;
    }

    public float getPercentage(String note){
        float f = 0.0f;
        switch (note) {
            case "do": return tabFrequencesNotes[0];
            case "do#": return tabFrequencesNotes[1];
            case "ré": return tabFrequencesNotes[2];
            case "ré#": return tabFrequencesNotes[3];
            case "mi": return tabFrequencesNotes[4];
            case "fa": return tabFrequencesNotes[5];
            case "fa#": return tabFrequencesNotes[6];
            case "sol": return tabFrequencesNotes[7];
            case "sol#": return tabFrequencesNotes[8];
            case "la": return tabFrequencesNotes[9];
            case "la#": return tabFrequencesNotes[10];
            case "si": return tabFrequencesNotes[11];
        }
        return f;
    }

    public float getOctaveMoyen(String note){
        int o = 0;
        switch (note) {
            case "do": return tabAvgOctaveNotes[0];
            case "do#": return tabAvgOctaveNotes[1];
            case "ré": return tabAvgOctaveNotes[2];
            case "ré#": return tabAvgOctaveNotes[3];
            case "mi": return tabAvgOctaveNotes[4];
            case "fa": return tabAvgOctaveNotes[5];
            case "fa#": return tabAvgOctaveNotes[6];
            case "sol": return tabAvgOctaveNotes[7];
            case "sol#": return tabAvgOctaveNotes[8];
            case "la": return tabAvgOctaveNotes[9];
            case "la#": return tabAvgOctaveNotes[10];
            case "si": return tabAvgOctaveNotes[11];
        }
        return o;
    }

    public void setNotes(List<Note> notes) {
        this.notes = notes;
    }

    public List<Note> getNotesObtenues() {
        return notesObtenues;
    }

    public void setNotesObtenues(List<Note> notesObtenues) {
        this.notesObtenues = notesObtenues;
    }

    public void setBpm(int bpm) {
        this.bpm = bpm;
    }

    public float getPercent_saxophone() {
        return percent_saxophone;
    }

    public void setPercent_saxophone(float percent_saxophone) {
        this.percent_saxophone = percent_saxophone;
    }

    public float getPercent_drums() {
        return percent_drums;
    }

    public void setPercent_drums(float percent_drums) {
        this.percent_drums = percent_drums;
    }

    public float getPercent_voice() {
        return percent_voice;
    }

    public void setPercent_voice(float percent_voice) {
        this.percent_voice = percent_voice;
    }

    public float getPercent_guitar() {
        return percent_guitar;
    }

    public void setPercent_guitar(float percent_guitar) {
        this.percent_guitar = percent_guitar;
    }

    public float getPercent_flute() {
        return percent_flute;
    }

    public void setPercent_flute(float percent_flute) {
        this.percent_flute = percent_flute;
    }

    public float getPercent_piano() {
        return percent_piano;
    }

    public void setPercent_piano(float percent_piano) {
        this.percent_piano = percent_piano;
    }

    public void setTabFrequencesNotes(float[] tabFrequencesNotes) {
        this.tabFrequencesNotes = tabFrequencesNotes;
    }

    public void setTabAvgOctaveNotes(float[] tabAvgOctaveNotes) {
        this.tabAvgOctaveNotes = tabAvgOctaveNotes;
    }

    public AudioSample[] getSamples() {
        return samples;
    }

    public void setSamples(AudioSample[] samples) {
        this.samples = samples;
    }

    @Override
    public String toString() {
        return getFile().getAbsolutePath();
    }
}
