package fr.mAIstro.audio;

import java.io.File;
import java.io.FileWriter;
import java.util.ArrayList;

/**
 * Liste de lecture de musiques les unes aprés les autres.
 */
public class Playlist {
    //nom de la playlist
    private String name;
    //playlist lancée ?
    boolean started = false;
    //morceau joué actuellement
    int currentTrack = 0;
    //musiques de la playlist
    ArrayList<Music> songs = new ArrayList<>();

    public Playlist(String name) {
        this.name = name;
    }

    /**
     * Fonction de démmarage de la playlist
     */
    public void play(){
        if(!started){
            started = true;
            songs.get(currentTrack).play();
        }
    }

    /**
     * fonction d'arret de la playlist
     */
    public void stop(){
        started = false;
        songs.get(currentTrack).stop();
    }

    /**
     * fonction de saut d'une musique
     */
    public void skip(){
        songs.get(currentTrack).stop();
        currentTrack++;
        if(currentTrack < songs.size()){
            songs.get(currentTrack).play();
        }
        else {
            currentTrack = 0;
            songs.get(0).play();
        }
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public ArrayList<Music> getSongs() {
        return songs;
    }

    public void setSongs(ArrayList<Music> songs) {
        this.songs = songs;
    }

    @Override
    public String toString() {
        return "Playlist{" +
                "name='" + name + '\'' +
                ", songs=" + songs +
                '}';
    }

    /**
     * fonction de sauvegarde de la playlist dans le chemin donné
     * @param filename chemin d'enregistrement
     */
    public void save(String filename){
        try {
            File file = new File("./src/main/resources/playlist/" + filename + ".plist");
            if (file.createNewFile()) {
                System.out.println("File created: " + file.getName());
            } else {
                System.out.println("File already exists.");
            }
            FileWriter myWriter = new FileWriter(file.getAbsoluteFile());
            myWriter.write(this.toString());
            myWriter.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
