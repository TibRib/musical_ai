package fr.mAIstro.audio;

import javax.sound.sampled.AudioFormat;
import javax.sound.sampled.AudioSystem;
import javax.sound.sampled.LineUnavailableException;
import javax.sound.sampled.SourceDataLine;

/**
 * Classe représentant une note de musique
 */
public class Note {
    // fréquences de chaque note de musique a l'octave 1
    public final static float[] NOTE_BASE_FREQUENCY = new float[] {
            65.41f,69.30f,73.42f,77.78f,82.41f,87.31f,92.50f,98.00f,103.83f,110.00f,116.54f,123.47f
    };

    //frequence de la note
    private int frequency;
    //nom de la note (do,ré,mi,...)
    private String name;
    //octave de la note
    private int octave;

    /**
     * crée une note en trouvant la fréquence la plus proche de celle d'entrée
     * @param frequency fréquence de la note entendue
     */
    public Note(int frequency){
        this.octave = findOctave(frequency);
        this.name = findName(frequency);

        this.frequency = (int) (getBaseFrequency(frequency) * Math.pow(2,octave-1));
    }

    /**
     * crée une note a partir de son nom et son octave, on retrouve donc sa fréquence
     * @param name nom de la note
     * @param octave octave de la note
     */
    public Note(String name, int octave){
        this.octave = octave;
        this.name = name;
        int base_id = 0;
        switch (name){
            case "do" : base_id = 0; break;
            case "do#" : base_id = 1; break;
            case "ré" : base_id = 2; break;
            case "ré#" : base_id = 3; break;
            case "mi" : base_id = 4; break;
            case "fa" : base_id = 5; break;
            case "fa#" : base_id = 6; break;
            case "sol" : base_id = 7; break;
            case "sol#" : base_id = 8; break;
            case "la" : base_id = 9; break;
            case "la#" : base_id = 10;break;
            case "si" : base_id = 11; break;
            default:
                System.out.println("Note non identifiée");
                return;
        }
        this.frequency = (int) (NOTE_BASE_FREQUENCY[base_id] * Math.pow(2,octave-1));
    }

    /**
     * trouve l'octave d'une note a partir de sa fréquence
     * @param frequency
     * @return octave
     */
    public static int findOctave(int frequency){
        int octave = 1;
        while(frequency > NOTE_BASE_FREQUENCY[0]*Math.pow(2,octave))
            octave++;
        if(Math.abs(frequency - NOTE_BASE_FREQUENCY[0]*Math.pow(2,octave)) < Math.abs(frequency - NOTE_BASE_FREQUENCY[11]*Math.pow(2,octave-1))) {
            octave++;
        }
        return octave;
    }

    /**
     * obtient la fréquence propre d'une note a partir de la frequence la plus proche de celle d'entrée
     * @param frequency fréquence entendue
     * @return fréquence propre
     */
    private float getBaseFrequency(int frequency){
        int base_freq = (int) (frequency/Math.pow(2,findOctave(frequency)-1));
        float mindelta = Math.abs(base_freq - NOTE_BASE_FREQUENCY[0]);
        int note_index = 0;
        for(int i=1;i<NOTE_BASE_FREQUENCY.length;i++) {
            float delta = Math.abs(base_freq - NOTE_BASE_FREQUENCY[i]);
            if(mindelta > delta){
                mindelta = delta;
                note_index = i;
            }
        }
        return NOTE_BASE_FREQUENCY[note_index];
    }

    /**
     * trouve le nom d'une note a partir de sa fréquence
     * @param frequency fréquence de la note
     */
    public String findName(int frequency){
        String ret = "";
        int base_freq = (int) (frequency/Math.pow(2,findOctave(frequency)-1));
        float mindelta = Math.abs(base_freq - NOTE_BASE_FREQUENCY[0]);
        int note_index = 0;
        for(int i=1;i<NOTE_BASE_FREQUENCY.length;i++) {
            float delta = Math.abs(base_freq - NOTE_BASE_FREQUENCY[i]);
            if(mindelta > delta){
                mindelta = delta;
                note_index = i;
            }
        }
        switch(note_index){
            case 0: return "do";
            case 1: return "do#";
            case 2: return "ré";
            case 3: return "ré#";
            case 4: return "mi";
            case 5: return "fa";
            case 6: return "fa#";
            case 7: return "sol";
            case 8: return "sol#";
            case 9: return "la";
            case 10: return "la#";
            case 11: return "si";
        }
        return null;
    }

    public int getFrequency() {
        return frequency;
    }

    public void setFrequency(int frequency) {
        this.frequency = frequency;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getOctave() {
        return octave;
    }

    public void setOctave(int octave) {
        this.octave = octave;
    }

    @Override
    public String toString() {
        return "Note{" +
                "frequency=" + frequency +
                ", name='" + name + '\'' +
                ", octave=" + octave +
                '}';
    }

    public boolean equals(Note n) {
       if(this.frequency == n.getFrequency() && this.octave == n.getOctave()){
           return true;
       }
       return false;
    }

    public void play(int frequenceEch, int bpm){
        byte[] buf = new byte[ 1 ];;
        AudioFormat af = new AudioFormat( (float )frequenceEch, 8, 1, true, false );
        SourceDataLine sdl = null;
        try {
            sdl = AudioSystem.getSourceDataLine( af );
            sdl.open();
            sdl.start();
            float bps =  (float)bpm/60;
            for( int i = 0; i < (float )frequenceEch/bps; i++ ) {
                double angle = i / ( (float )frequenceEch / this.frequency ) * 2.0 * Math.PI;
                buf[ 0 ] = (byte )( Math.sin( angle ) * 100 );
                sdl.write( buf, 0, 1 );
            }
            sdl.drain();
            sdl.stop();
        } catch (LineUnavailableException e) {
            e.printStackTrace();
            System.out.println("Audio Line of system not available");
        }

    }

}
