package fr.mAIstro.UI.Panels;

import fr.mAIstro.ai.neuralnetwork.Network;
import fr.mAIstro.ai.neuralnetwork.NetworkSig;
import fr.mAIstro.ai.neuralnetwork.implementations.AI_Shmilblick;
import javafx.scene.Scene;
import javafx.scene.layout.BorderPane;
import javafx.stage.Stage;

//Fenetre de réseau de neuronne
public class NeuralNetworkPane {

    public NeuralNetworkPane(NetworkSig ai) {
        Stage stage = new Stage();
        BorderPane pane = new BorderPane();
        NetworkCanvas canvas = new NetworkCanvas(stage,ai);
        pane.setCenter(canvas);

        stage.setScene(new Scene(pane, 1920, 1080));
        stage.show();
        canvas.redraw();

    }
}
