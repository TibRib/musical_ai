package fr.mAIstro.UI.Panels;

import javafx.application.Platform;
import javafx.event.EventHandler;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.image.Image;
import javafx.stage.Stage;
import javafx.stage.WindowEvent;

//Fenetre d'accueil
public class AcceuilPanel {


    public AcceuilPanel(Stage stage) throws Exception {
        Parent root = FXMLLoader.load(getClass().getResource("accueil_windows.fxml"));

        Scene scene = new Scene(root);

        stage.setTitle("mAIstro");
        stage.getIcons().add(new Image("fr/mAIstro/UI/Panels/img/LOGO-mAIstro.png"));
        stage.setScene(scene);
        stage.show();
    }
}
