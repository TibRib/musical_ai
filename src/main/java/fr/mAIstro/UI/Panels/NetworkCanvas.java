package fr.mAIstro.UI.Panels;


import fr.mAIstro.ai.neuralnetwork.Network;
import fr.mAIstro.ai.neuralnetwork.NetworkPanel;
import javafx.scene.canvas.Canvas;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.paint.Color;
import javafx.stage.Stage;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;

/**
 * Panneau de dessin JavaFX représentant un réseau de neurone graphiquement
 */
public class NetworkCanvas extends Canvas {
    //réseau de neurone cible
    Network network;

    // Inner-class servant a effectuer des calculs sur les positions des différents éléments constituant le réseau de neurones
    private class NetworkGraphInfo{
        //largeur du panneau
        public int w;
        //hauteur du panneau
        public int h;

        //nombre de couches cachées du réseau de neurones
        public int nbcouchecache;
        //nombre total de couches de neurones
        public int nbcouches;
        //nombre d'entrées
        public int nbinput;
        //nombre de sorties
        public int nboutput;
        //nombre maximu de neurone sur une couche du réseau
        public int maxnbneurone;

        public int[] nbneuronecache;

        //position des centres des cercles représentant les neurones
        public ArrayList<ArrayList<int[]>> centers;
        //rayon des cercles
        public int r;
        //distance en longueur entre les neurones
        public int dx;

        //largeur totale de la representation graphique
        public int fullwidth;

        //poids maximum et minimum des neurones
        public float min_weight,max_weight;

        public NetworkGraphInfo(Canvas pan){
            w = (int)pan.getWidth();
            h = (int)pan.getHeight();

            min_weight = network.getMinWeight();
            max_weight = network.getMaxWeight();

            nbcouchecache = network.getCouchesCachees().size();
            nbcouches = 2 + nbcouchecache;
            nbinput = network.getNbInput();
            nboutput = network.getCoucheSortie().size();
            maxnbneurone = Math.max(nbinput,nboutput);

            nbneuronecache = new int[nbcouchecache];
            for(int i=0;i<nbcouchecache;i++){
                nbneuronecache[i] = network.getCouchesCachees().get(i).size();
                maxnbneurone = Math.max(maxnbneurone,nbneuronecache[i]);
            }

            centers = new ArrayList<>();
            for(int i = 0;i<nbcouches;i++)
                centers.add(new ArrayList<>());

            r = w/27;
            dx = (int)(r*3f);

            fullwidth = r + (nbcouches-1)*dx ;

            for(int i=0;i<network.getNbInput();i++){
                int[] center = {w/2 - fullwidth/2 +r/2,h-(i+2)*(h/(nbinput+2))+r/2};
                centers.get(0).add(center);
            }

            for(int i=0;i<nbcouchecache;i++){
                for(int i2=0;i2<nbneuronecache[i];i2++) {
                    int[] center = {w / 2 - fullwidth / 2 + (i + 1) * dx+r/2,h - (i2 + 2) * (h / (nbneuronecache[i] + 2))+r/2};
                    centers.get(i+1).add(center);
                }
            }

            for(int i=0;i<network.getCoucheSortie().size();i++){
                int[] center = {w/2 - fullwidth/2 + (nbcouchecache+1)*dx +r/2,h-(i+2)*(h/(nboutput+2))+r/2};
                centers.get(nbcouches-1).add(center);
            }
        }
    }

    //constructeur
    public NetworkCanvas (Stage stage,Network n){
        super(stage.getWidth(),stage.getHeight());
        //si la fenetre change de taille, on doit redessiner le réseau
        stage.widthProperty().addListener((obs, oldVal, newVal) -> {
            this.setWidth(newVal.intValue());
            this.redraw();
        });
        //si la fenetre change de taille, on doit redessiner le réseau
        stage.heightProperty().addListener((obs, oldVal, newVal) -> {
            this.setHeight(newVal.intValue());
            this.redraw();
        });
        this.network = n;
        redraw();
    }

    public void redraw(){
        GraphicsContext g = this.getGraphicsContext2D();

        NetworkGraphInfo info = new NetworkGraphInfo(this);
        g.clearRect(0,0,info.w,info.h);

        //dessin des entrées en bleu
        g.setFill(Color.BLUE);
        for(int i=0;i<network.getNbInput();i++){
            int cx = info.centers.get(0).get(i)[0];
            int cy = info.centers.get(0).get(i)[1];
            g.fillOval(cx-info.r/2, cy-info.r/2,info.r,info.r);
        }

        //dessin des couches cachées en noir
        for(int i=0;i<info.nbcouchecache;i++){
            for(int i2=0;i2<info.nbneuronecache[i];i2++) {
                int cx = info.centers.get(i + 1).get(i2)[0];
                int cy = info.centers.get(i + 1).get(i2)[1];
                g.setFill(Color.BLACK);
                g.fillOval(cx-info.r/2, cy-info.r/2,info.r,info.r);
                int i3 = 0;
                for(int[] point : info.centers.get(i)){
                    //dessin des liens inter-neuronaux
                    float borne = info.max_weight - info.min_weight;
                    float nb = network.getCouchesCachees().get(i).get(i2).getPoids()[i3] - info.min_weight;
                    g.setStroke(Color.BLACK);
                    //plus le poids est fort en comparaison des autres poids, plus le trait est épais
                    g.setLineWidth(6*(1.f*nb/borne)+1);
                    g.strokeLine(cx,cy,point[0],point[1]);
                    i3++;
                }
            }
        }

        //dessin des sorties en rouge
        g.setFill(Color.RED);
        for(int i=0;i<network.getCoucheSortie().size();i++){
            int cx = info.centers.get(info.nbcouches-1).get(i)[0];
            int cy = info.centers.get(info.nbcouches-1).get(i)[1];
            g.setStroke(Color.RED);
            g.fillOval(cx-info.r/2, cy-info.r/2,info.r,info.r);
            int i3 = 0;
            for(int[] point : info.centers.get(info.nbcouches-2)){
                //dessin des liens inter-neuronaux
                float borne = info.max_weight - info.min_weight;
                float nb = network.getCoucheSortie().get(i).getPoids()[i3] - info.min_weight;
                g.setStroke(Color.RED);
                //plus le poids est fort en comparaison des autres poids, plus le trait est épais
                g.setLineWidth(6*(1.f*nb/borne)+1);
                g.strokeLine(cx,cy,point[0],point[1]);
                i3++;
            }
        }
        g.setLineWidth(1);
        g.strokeText(LocalDateTime.now().format(DateTimeFormatter.ISO_DATE_TIME),100,100);
    }
}
