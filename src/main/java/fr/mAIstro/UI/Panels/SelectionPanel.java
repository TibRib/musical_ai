package fr.mAIstro.UI.Panels;

import javafx.application.Platform;
import javafx.event.EventHandler;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;
import javafx.stage.WindowEvent;

//Fenetre de selection
public class SelectionPanel {

    public SelectionPanel(Stage stage) throws Exception {
        Parent root = FXMLLoader.load(getClass().getResource("selection_windows.fxml"));

        Scene scene = new Scene(root);
        stage.setTitle("Selection Audio");
        stage.setScene(scene);
        stage.show();


    }
}