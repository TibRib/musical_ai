package fr.mAIstro.UI.Panels;

import fr.mAIstro.UI.Controller.RealtimeFFTController;
import fr.mAIstro.UI.Controller.VisualizerController;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.image.Image;
import javafx.stage.Modality;
import javafx.stage.Stage;
import javafx.stage.StageStyle;

public class RealtimeFFTPanel {

    public RealtimeFFTPanel() throws Exception {
        Stage stage = new Stage();

        //remove window decoration
        stage.initStyle(StageStyle.UNDECORATED);
        FXMLLoader loader=new FXMLLoader();
        Parent root = loader.load(getClass().getResource("FFTVisualizer.fxml"));


        Scene scene = new Scene(root);

        stage.setTitle("FFT : temps réel ("+ RealtimeFFTController.music.getFile().getName()+")");
        stage.getIcons().add(new Image("fr/mAIstro/UI/Panels/img/LOGO-mAIstro.png"));
        stage.setScene(scene);
        stage.initModality(Modality.APPLICATION_MODAL);
        stage.show();
    }
}
