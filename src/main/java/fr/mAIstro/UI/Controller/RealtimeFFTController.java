package fr.mAIstro.UI.Controller;

import fr.mAIstro.audio.AudioSample;
import fr.mAIstro.audio.Music;
import javafx.application.Platform;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.chart.*;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.Slider;
import javafx.scene.control.ToolBar;
import javafx.scene.input.MouseEvent;
import javafx.stage.Stage;

import java.net.URL;
import java.util.ArrayList;
import java.util.ResourceBundle;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

public class RealtimeFFTController implements Initializable {

    @FXML
    private Label FreqEchLabel;

    @FXML
    private Label dureeLabel;

    @FXML
    private Label channelsLabel;

    @FXML
    private Label titleLabel;

    @FXML
    private Label folderLabel;

    @FXML
    private LineChart<Number, Number> fftChart;

    @FXML
    private Slider slider_duration;

    @FXML
    private ToolBar mainToolBar;

    private double xOffset = 0;
    private double yOffset = 0;

    final int UPDATE_TIME_MS = 80;
    private boolean playing = false;
    private ScheduledExecutorService scheduledExecutorService;

    //Musique en séléction
    public static Music music;

    XYChart.Series<Number, Number> series;

    @Override
    public void initialize(URL url, ResourceBundle rb) {
        //fftChart.getXAxis().setVisible(false);
        fftChart.getYAxis().setVisible(false);
        fftChart.setAnimated(true);

        System.out.println("Visualiseur FFT initialisé");
        music = new Music(SelectionController.pathFileSelected);
        music.associeNotes();
        System.out.println(music.toString());
        series = new XYChart.Series<>();

        if(music != null) {
            FreqEchLabel.setText("Fréquence d'échantillonnage : "+ music.getFrequency());
            //dureeLabel.setText("Durée : "+(music.getFrequency() * music.getSamples().length)+" sec.");
            channelsLabel.setText("Nombre de channels : " + music.getNb_channels());
            String path = SelectionController.pathFileSelected.toString();
            String[] pathcomponents = path.split("\\\\");
            String titlewav = pathcomponents[pathcomponents.length-1];
            String title = titlewav.substring(0, titlewav.lastIndexOf('.'));
            titleLabel.setText(title);
            folderLabel.setText(path);
            float duree=(float)music.getData().length/(music.getFrequency()*music.getNb_channels());
            dureeLabel.setText(duree + " s");
        }
        else{
            System.out.println("Erreur : musique non reconnue");
        }

        fftChart.getData().add(series);
        slider_duration.valueProperty().addListener(new ChangeListener<Number>() {
            @Override
            public void changed(ObservableValue<? extends Number> observable, Number oldValue, Number newValue) {
                music.setPlaybackPercent(newValue.floatValue());
            }
        });

        if(mainToolBar != null) {
            mainToolBar.setOnMousePressed(new EventHandler<MouseEvent>() {
                @Override
                public void handle(MouseEvent event) {
                    Stage stage = (Stage) mainToolBar.getScene().getWindow();
                    xOffset = event.getSceneX();
                    yOffset = event.getSceneY();
                }
            });
            mainToolBar.setOnMouseDragged(new EventHandler<MouseEvent>() {
                @Override
                public void handle(MouseEvent event) {
                    Stage stage = (Stage) mainToolBar.getScene().getWindow();
                    stage.setX(event.getScreenX() - xOffset);
                    stage.setY(event.getScreenY() - yOffset);
                }
            });
        }
    }


    public void actionStop(ActionEvent actionEvent) {
        music.stop();
        if(scheduledExecutorService != null)
            scheduledExecutorService.shutdownNow();
    }

    public void actionPlay(ActionEvent actionEvent) {
        scheduledExecutorService = Executors.newSingleThreadScheduledExecutor();

        scheduledExecutorService.scheduleAtFixedRate(() -> {
            Platform.runLater(() -> {
                final int idcur = (int) Math.floor(music.getCurrent_index() / (double)AudioSample.SIZE);
                if(music != null && music.getSamples() != null){
                    if(idcur < music.getSamples().length) {
                        if (music.getSamples()[idcur].isSilent() == false)
                            music.getSamples()[idcur].setSerieGraphique(series);
                    }
                }else {
                    System.out.println("Echantillon complexe non existant ! Verifiez l'appel à associeNotes()");
                }
            });
        }, 0, UPDATE_TIME_MS, TimeUnit.MILLISECONDS);

        music.play();
    }


    public void handleClose(ActionEvent actionEvent) {
        actionStop(actionEvent);
        ((Stage)((Button)actionEvent.getSource()).getScene().getWindow()).close();
    }

    public void handleMaximize(ActionEvent actionEvent) {
        Stage window = ((Stage)((Button)actionEvent.getSource()).getScene().getWindow());
        if(window.isMaximized())
            window.setMaximized(false);
        else
            window.setMaximized(true);
    }

    public void handleMinimize(ActionEvent actionEvent){
        Stage window = ((Stage)((Button)actionEvent.getSource()).getScene().getWindow());
        if(window.isIconified())
            window.setIconified(false);
        else
            window.setIconified(true);
    }


}
