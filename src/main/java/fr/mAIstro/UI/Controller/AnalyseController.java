package fr.mAIstro.UI.Controller;

import fr.mAIstro.UI.Panels.NeuralNetworkPane;
import fr.mAIstro.ai.neuralnetwork.implementations.AI_Genre;
import fr.mAIstro.ai.neuralnetwork.implementations.AI_Instrument;
import fr.mAIstro.ai.neuralnetwork.implementations.AI_Shmilblick;
import fr.mAIstro.audio.Music;
import javafx.concurrent.Task;
import javafx.concurrent.WorkerStateEvent;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.geometry.Side;
import javafx.scene.Parent;
import javafx.scene.chart.PieChart;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.stage.Stage;

import java.io.*;
import java.net.URL;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.ResourceBundle;

public class AnalyseController implements Initializable {

    @FXML
    private PieChart statInstru;
    @FXML
    private PieChart statGenre;
    @FXML
    private Label nameSound;

    public static String topGenre;
    private File selected_file;
    private AI_Genre ai_genre;
    private AI_Instrument ai_instrument;

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        selected_file = new File(SelectionController.pathFileSelected);
        nameSound.setText(selected_file.getName());
        ai_genre = new AI_Genre("./src/main/resources/SavedNeuralNetworks/IA_genre.txt");
        ai_instrument = new AI_Instrument("./src/main/resources/SavedNeuralNetworks/IA_instruments.txt");
        loadStats();
        //On genère les dossiers de sortie si la sortie à changer
        genereDossier();
        //Copy du fichier dans le bon dossier dans un nouveau thread
        copyFile(false);

    }

    //Permet de creer les PieChart en fonction des résultats des IA Instrument et IA genre
    public void loadStats(){
        ArrayList<Float> probaInstru = ai_instrument.solve(new Music(selected_file.getAbsolutePath()));

        for(int i=0;i<probaInstru.size();i++){
            statInstru.getData().add(new PieChart.Data(ai_instrument.getOutput_name().get(i),probaInstru.get(i)*100));
        }
        statInstru.setScaleX(1.2);
        statInstru.setScaleY(1.2);


        ArrayList<Float> probaGenre = ai_genre.solve(probaInstru,new Music(selected_file.getAbsolutePath()));
        float max=0;
        for(int i=0;i<probaGenre.size();i++){
            if(probaGenre.get(i)>max){
                topGenre=ai_genre.getOutput_name().get(i);
            }
            statGenre.getData().add(new PieChart.Data(ai_genre.getOutput_name().get(i),probaGenre.get(i)*100));
        }
        statGenre.setScaleX(1.2);
        statGenre.setScaleY(1.2);
    }

    //Permet de generer les dossier sur le dossier de sortie
    public static void genereDossier(){
        ArrayList<String> genrelist = new ArrayList<String>(Arrays.asList("Classique", "Electronique", "Jazz", "Metal","Rap", "Rock"));
        for(String s : genrelist){
            File file = new File(SelectionController.pathFileOutput + "\\" + s);
            if(!file.exists()){
                if(file.mkdir()){
                    System.out.println("Dossier " + s + "créé");
                }
                else{
                    System.out.println("Dossier non créé");
                }
            }
        }
    }

    //Permet de copier un fichier
    public static void copyFile(boolean isDossier){
        //Si on est avec un seul fichier on utilse le thread
        if(!isDossier) {
            Task<Parent> taskcopie = new Task<Parent>() {
                @Override
                protected Parent call() throws Exception {
                    InputStream input = null;
                    OutputStream output = null;
                    try {
                        input = new FileInputStream(SelectionController.pathFileSelected);
                    } catch (FileNotFoundException e) {
                        e.printStackTrace();
                    }
                    try {
                        output = new FileOutputStream(SelectionController.pathFileOutput + "\\" + AnalyseController.topGenre + "\\" + new File(SelectionController.pathFileSelected).getName());
                    } catch (FileNotFoundException e) {
                        e.printStackTrace();
                    }
                    if (input != null && output != null) {
                        try {
                            copy(input, output);
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                    }
                    return null;
                }
            };
            taskcopie.setOnSucceeded(new EventHandler<WorkerStateEvent>() {
                @Override
                public void handle(WorkerStateEvent event) {
                    System.out.println("Copie Fini");
                }
            });
            Thread copythread = new Thread(taskcopie);
            copythread.start();
        }
        //Si c'est un dossier alors on fait sans thread
        else{
            genereDossier();
            InputStream input = null;
            OutputStream output = null;
            try {
                input = new FileInputStream(SelectionController.pathFileSelected);
            } catch (FileNotFoundException e) {
                e.printStackTrace();
            }
            try {
                output = new FileOutputStream(SelectionController.pathFileOutput + "\\" + AnalyseController.topGenre + "\\" + new File(SelectionController.pathFileSelected).getName());
            } catch (FileNotFoundException e) {
                e.printStackTrace();
            }
            if (input != null && output != null) {
                try {
                    copy(input, output);
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }

    }

    //Permet de copier bit à bit
    public static void copy(InputStream source, OutputStream destination) throws IOException {
        try {
            int c;
            while ((c = source.read()) != -1) {
                destination.write(c);
            }
        } finally {
            if (source != null) {
                source.close();
            }
            if (destination != null) {
                destination.close();
            }
        }
    }

    //Permet de mettre la fenêtre en pleine écran
    public void handleMaximize(ActionEvent actionEvent) {
        Stage window = ((Stage)((Button)actionEvent.getSource()).getScene().getWindow());
        if(window.isMaximized())
            window.setMaximized(false);
        else
            window.setMaximized(true);
    }

    //Permet de fermer la fenêtre en appuyant sur la croix en haut à droite
    public void handleClose(ActionEvent actionEvent) {
        ((Stage)((Button)actionEvent.getSource()).getScene().getWindow()).close();
    }

    //Permet de réduire la fenêtre
    public void handleMinimize(ActionEvent actionEvent) {
        Stage window = ((Stage)((Button)actionEvent.getSource()).getScene().getWindow());
        if(window.isIconified())
            window.setIconified(false);
        else
            window.setIconified(true);
    }

    //Permet d'afficher les réseaux de neurone Genre ou Instrument.
    public void displayNeuralNetwork(ActionEvent actionEvent) {
        if(((Button)actionEvent.getSource()).getId().equals("buttonNeuroneGenre")){
            new NeuralNetworkPane(ai_genre);
        }
        else if(((Button)actionEvent.getSource()).getId().equals("buttonNeuroneInstru")){
            new NeuralNetworkPane(ai_instrument);
        }
    }
}
