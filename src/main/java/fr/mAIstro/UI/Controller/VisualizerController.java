package fr.mAIstro.UI.Controller;

import fr.mAIstro.audio.Music;
import javafx.application.Platform;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.chart.AreaChart;
import javafx.scene.chart.XYChart;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.Slider;
import javafx.scene.control.ToolBar;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseEvent;
import javafx.stage.Stage;

import java.net.URL;
import java.util.ArrayList;
import java.util.ResourceBundle;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

public class VisualizerController implements Initializable {

    @FXML
    private Label FreqEchLabel;

    @FXML
    private Label dureeLabel;

    @FXML
    private Label channelsLabel;

    @FXML
    private Label titleLabel;

    @FXML
    private Label folderLabel;

    @FXML
    private AreaChart<Number, Number> graphiqueXY;

    @FXML
    private Slider slider_duration;

    @FXML
    private ToolBar mainToolBar;

    @FXML
    private Button playButton;

    @FXML
    private ImageView playButtonIcon;

    private double xOffset = 0;
    private double yOffset = 0;

    final int WINDOW_SIZE = 100;
    final int UPDATE_TIME_MS = 16;
    private boolean playing = false;
    private ScheduledExecutorService scheduledExecutorService;

    //Musique en séléction
    public static Music music;

    XYChart.Series<Number, Number> series;
    int cpt =0;

    @Override
    public void initialize(URL url, ResourceBundle rb) {
        graphiqueXY.getXAxis().setVisible(false);

        graphiqueXY.getYAxis().setVisible(false);
        graphiqueXY.setAnimated(false);

        System.out.println("Visualiseur initialisé");
        music = new Music(SelectionController.pathFileSelected);
        series = new XYChart.Series<>();

        if(music != null) {
            FreqEchLabel.setText("Fréquence d'échantillonnage : "+ music.getFrequency());
            channelsLabel.setText("Nombre de channels : " + music.getNb_channels());
            String path = SelectionController.pathFileSelected.toString();
            String[] pathcomponents = path.split("\\\\");
            String titlewav = pathcomponents[pathcomponents.length-1];
            String title = titlewav.substring(0, titlewav.lastIndexOf('.'));
            titleLabel.setText(title);
            folderLabel.setText(path);
            float duree=(float)music.getData().length/(music.getFrequency()*music.getNb_channels());
            dureeLabel.setText("Durée : "+ duree + " s");
        }
        else{
            System.out.println("Erreur : musique non reconnue");
        }

        graphiqueXY.getData().add(series);
        slider_duration.valueProperty().addListener(new ChangeListener<Number>() {
            @Override
            public void changed(ObservableValue<? extends Number> observable, Number oldValue, Number newValue) {
                music.setPlaybackPercent(newValue.floatValue());
            }
        });

        if(mainToolBar != null) {
            mainToolBar.setOnMousePressed(new EventHandler<MouseEvent>() {
                @Override
                public void handle(MouseEvent event) {
                    Stage stage = (Stage) mainToolBar.getScene().getWindow();
                    xOffset = event.getSceneX();
                    yOffset = event.getSceneY();
                }
            });
            mainToolBar.setOnMouseDragged(new EventHandler<MouseEvent>() {
                @Override
                public void handle(MouseEvent event) {
                    Stage stage = (Stage) mainToolBar.getScene().getWindow();
                    stage.setX(event.getScreenX() - xOffset);
                    stage.setY(event.getScreenY() - yOffset);
                }
            });
        }
    }


    public void actionStop(ActionEvent actionEvent) {
        music.stop();
        if(scheduledExecutorService != null)
            scheduledExecutorService.shutdownNow();
    }

    public void actionPause(ActionEvent actionEvent){
        Platform.runLater(() -> {
            Image curicon = playButtonIcon.getImage();
            String urlPath = curicon.getUrl().substring(0,curicon.getUrl().lastIndexOf('/')+1);
            playButtonIcon.setImage(new Image(urlPath + "play.png",curicon.getWidth(),curicon.getWidth(),true,false));
            playButton.setOnAction(this::actionPlay);
        });
        actionStop(actionEvent);
    }

    public void actionPlay(ActionEvent actionEvent) {
        Integer increment = music.getData().length / UPDATE_TIME_MS;
        scheduledExecutorService = Executors.newSingleThreadScheduledExecutor();

        Integer finalIncrement = increment;

        var ref = new Object() {
            double lastpercent = 0;
        };
        scheduledExecutorService.scheduleAtFixedRate(() -> {
            Platform.runLater(() -> {

                cpt += finalIncrement;
                float amplitude = Math.abs(music.getCurrent_byte() ) / 0x8000;

                if(ref.lastpercent != (int)music.getCurrent_index()*100/music.getData().length) {
                    slider_duration.setValue(Math.abs(music.getCurrent_index() * 100 / music.getData().length));
                }
                ref.lastpercent = music.getCurrent_index()*100/music.getData().length;

                //int offset = series.getData().size() > 0 ?series.getData().get(0).getXValue().intValue() : 0;
                if (series.getData().size() > WINDOW_SIZE+1) {
                    series.getData().remove(0);
                }
                ArrayList<Float> amplitudes = new ArrayList<>();
                for(int i=0;i<series.getData().size();i++){
                    amplitudes.add(series.getData().get(i).getYValue().floatValue());
                }
                amplitudes.add(amplitude);
                series.getData().clear();
                for(int i =0;i<amplitudes.size();i++){
                    series.getData().add(new XYChart.Data<>(i,amplitudes.get(i)));
                }
            });
        }, 0, UPDATE_TIME_MS, TimeUnit.MILLISECONDS);

        music.play();
        Platform.runLater(() -> {
            Image curicon = playButtonIcon.getImage();
            String urlPath = curicon.getUrl().substring(0,curicon.getUrl().lastIndexOf('/')+1);
            playButtonIcon.setImage(new Image(urlPath + "pause.png",curicon.getWidth(),curicon.getWidth(),true,false));
            playButton.setOnAction(this::actionPause);
        });

    }


    public void handleClose(ActionEvent actionEvent) {
        actionStop(actionEvent);
        ((Stage)((Button)actionEvent.getSource()).getScene().getWindow()).close();
    }

    public void handleMaximize(ActionEvent actionEvent) {
        Stage window = ((Stage)((Button)actionEvent.getSource()).getScene().getWindow());
        if(window.isMaximized())
            window.setMaximized(false);
        else
            window.setMaximized(true);
    }

    public void handleMinimize(ActionEvent actionEvent){
        Stage window = ((Stage)((Button)actionEvent.getSource()).getScene().getWindow());
        if(window.isIconified())
            window.setIconified(false);
        else
            window.setIconified(true);
    }


}
