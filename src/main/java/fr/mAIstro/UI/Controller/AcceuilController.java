package fr.mAIstro.UI.Controller;

import fr.mAIstro.UI.Panels.SelectionPanel;
import javafx.event.ActionEvent;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.stage.Stage;

import java.net.URL;
import java.util.ResourceBundle;

public class AcceuilController implements Initializable {

    @Override
    public void initialize(URL location, ResourceBundle resources) {

    }

    //Action pour aller dans la fenetre de selection
    public void letsGo(ActionEvent actionEvent) throws Exception {
        Stage window = ((Stage)((Button)actionEvent.getSource()).getScene().getWindow());
        SelectionPanel s=new SelectionPanel(window);
    }

    //Permet de mettre la fenêtre en pleine écran
    public void handleMaximize(ActionEvent actionEvent) {
        Stage window = ((Stage)((Button)actionEvent.getSource()).getScene().getWindow());
        if(window.isMaximized())
            window.setMaximized(false);
        else
            window.setMaximized(true);
    }

    //Permet de fermer la fenêtre et le programme
    public void handleClose(ActionEvent actionEvent) {
        ((Stage)((Button)actionEvent.getSource()).getScene().getWindow()).close();
        System.exit(0);
    }

    //Permet de réduire la fenêtre
    public void handleMinimize(ActionEvent actionEvent) {
        Stage window = ((Stage)((Button)actionEvent.getSource()).getScene().getWindow());
        if(window.isIconified())
            window.setIconified(false);
        else
            window.setIconified(true);
    }
}
