package fr.mAIstro.UI.Controller;

import fr.mAIstro.UI.Panels.AnalysePanel;
import fr.mAIstro.UI.Panels.VisualizerPanel;
import fr.mAIstro.ai.neuralnetwork.implementations.AI_Genre;
import fr.mAIstro.ai.neuralnetwork.implementations.AI_Instrument;
import fr.mAIstro.audio.Music;
import javafx.application.Platform;
import javafx.concurrent.Task;
import javafx.concurrent.WorkerStateEvent;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.chart.XYChart;
import javafx.scene.control.*;
import javafx.scene.effect.DropShadow;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.stage.DirectoryChooser;
import javafx.stage.Modality;
import javafx.stage.Stage;

import java.io.*;
import java.net.URL;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.ResourceBundle;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

public class SelectionController implements Initializable {

    //Init Explo fichier
    private final DirectoryChooser directoryChooser = new DirectoryChooser();

    //Chemin du dossier du browser Input
    private String pathFile="/";

    public static String pathFileOutput="src/main/resources/result";

    private Label labelSelected=new Label("");

    //Chemin du fichier sélectionné
    public static String pathFileSelected;

    private File[] listfile;

    private File[] listfileAnalyse;
    float[] rgb = new float[3];
    boolean[] rgbMultipliers = new boolean[3];

    private final float COEF_RGB = 0.01f;

    @FXML
    private VBox Inputbrowser;
    @FXML
    private VBox Outputbrowser;

    @FXML
    private TextField textFieldInput;

    @FXML
    private TextField textFieldOutput;

    @FXML
    private Label nameSound;

    @FXML
    private ImageView albumPicture;

    @FXML
    private ProgressBar progressBar;

    @FXML
    private AnchorPane pane;

    @FXML
    private ToolBar mainToolBar;

    private double xOffset = 0;
    private double yOffset = 0;

    private boolean loading=false;

    //Affiche les fichier dans un browser
    public void afficheFileInBrowser(TextField textField,VBox browser,File File,boolean output){
        browser.getChildren().clear();
        listfile = File.listFiles(pathname -> {
            if(pathname.isDirectory()){
                return true;
            }
            if(pathname.getAbsolutePath().endsWith(".wav")){
                return true;
            }
            return false;
        });
        // On ajouter un dossier pour retourner à la racine
        Label retour=new Label("../");
        retour.setGraphic(new ImageView(new Image("fr/mAIstro/UI/Panels/img/folder.png", 15, 15, false, true)));
        browser.getChildren().add(retour);
        //Action dossier retour
        retour.setOnMouseClicked(e->{
                File f;
                if(!output) {
                   f =new File(pathFile).getParentFile();
                    if(f!=null && !loading) {
                        pathFile = f.getAbsolutePath();
                        textField.setText(pathFile);
                        afficheFileInBrowser(textField, browser, f,output);
                    }
                }
                else{
                     f =new File(pathFileOutput).getParentFile();
                    if(f!=null && !loading) {
                        pathFileOutput = f.getAbsolutePath();
                        textField.setText(pathFileOutput);
                        afficheFileInBrowser(textField, browser, f,output);
                    }
                }

        });
        for(File f : listfile) {
            Label l = new Label(f.getName());
            if (f.isDirectory()) {
                l.setGraphic(new ImageView(new Image("fr/mAIstro/UI/Panels/img/folder.png", 15, 15, false, true)));
                //Action pour avancer dans un dossier
                l.setOnMouseClicked(e-> {
                    if(!loading) {
                        if (!output) {
                            pathFile = f.getAbsolutePath();
                            textField.setText(pathFile);
                        } else {
                            pathFileOutput = f.getAbsolutePath();
                            textField.setText(pathFileOutput);
                        }
                        afficheFileInBrowser(textField, browser, f, output);
                    }
                });
            } else {
                l.setGraphic(new ImageView(new Image("fr/mAIstro/UI/Panels/img/file.png", 15, 15, false, true)));

                //On ajoute un event pour chaque label + on change la couleur du file sélectionner
                l.setOnMouseClicked(e->{
                    //Double click
                    if(e.getClickCount()>=2 && !loading){
                        if(output) {
                            pathFileSelected = f.getAbsolutePath();
                        }
                        //On essaye d'afficher le visualiseur
                        try {
                            VisualizerPanel v = new VisualizerPanel();
                        } catch (Exception ex) {
                            ex.printStackTrace();
                        }

                    }
                    //Si le label selectionné est different et qui n'est pas dans le browser de sortie et qu'on est pas en chargement
                    if(!labelSelected.getText().equals(l.getText()) && !output && !loading) {
                        nameSound.setText(l.getText());
                        pathFileSelected = f.getAbsolutePath();
                        l.setStyle("-fx-background-color:POWDERBLUE");
                        int i = 0;
                        i = browser.getChildren().indexOf(labelSelected);
                        if (i != -1) {
                            Label tempLabel = (Label) browser.getChildren().get(i);
                            tempLabel.setStyle("");
                        }
                        labelSelected = l;
                    }
                });

            }
            browser.getChildren().add(l);
        }
    }




    //Action sur le bouton de choix de dossier d'entrée
    public void validPathInput(ActionEvent actionEvent) {
        directoryChooser.setInitialDirectory(new File(pathFile));
        File file=directoryChooser.showDialog(textFieldInput.getScene().getWindow());
        if(file !=null){
            pathFile=file.getAbsolutePath();
            textFieldInput.setText(pathFile);
            afficheFileInBrowser(textFieldInput,Inputbrowser,file,false);
        }

    }

    //Action sur le bouton d'analyse de sélection
    public void actionSelect(ActionEvent actionEvent) {
        if(pathFileSelected!=null) {
            try {
                AnalysePanel a = new AnalysePanel();
            } catch (Exception e) {
                e.printStackTrace();
            }
            progressBar.setProgress(1.0f);
            afficheFileInBrowser(textFieldOutput,Outputbrowser,new File(pathFileOutput),true);

        }
    }

    //Action sur le bouton d'analyse d'un dossier
    public void actionDossier(ActionEvent actionEvent) {
        if(!loading) {
            loading=true;
            //Tache d'analyse du dossier entier + lancement du chargement
            Task<Parent> task = new Task<Parent>() {
                @Override
                protected Parent call() throws Exception {
                    AI_Genre ai_genre = new AI_Genre("./src/main/resources/SavedNeuralNetworks/IA_genre.txt");
                    AI_Instrument ai_instrument = new AI_Instrument("./src/main/resources/SavedNeuralNetworks/IA_instruments.txt");
                    ArrayList<Float> probaGenre;
                    ArrayList<Float> probaInstru;
                    String topGenre = null;
                    File file = new File(pathFile);
                    listfileAnalyse = file.listFiles();
                    int nbfichierwav = 0;
                    for (File f : listfileAnalyse) {
                        if (f.getAbsolutePath().endsWith("wav")) {
                            nbfichierwav++;
                        }
                    }
                    int j = 0;
                    if(nbfichierwav!=0) {
                        for (File f : listfileAnalyse) {
                            if (!f.isDirectory() && f.getAbsolutePath().endsWith("wav")) {
                                //Update du texte du label son actuel
                                Platform.runLater(new Runnable() {
                                    @Override
                                    public void run() {
                                       nameSound.setText(f.getName());
                                        afficheFileInBrowser(textFieldOutput,Outputbrowser,new File(pathFileOutput),true);
                                    }
                                });
                                //incrementation de la progresse bar
                                updateProgress(j, nbfichierwav);
                                probaInstru = ai_instrument.solve(new Music(f.getAbsolutePath()));
                                probaGenre = ai_genre.solve(probaInstru, new Music(f.getAbsolutePath()));
                                float max = 0;
                                for (int i = 0; i < probaGenre.size(); i++) {
                                    if (probaGenre.get(i) > max) {
                                        topGenre = ai_genre.getOutput_name().get(i);

                                    }
                                }
                                AnalyseController.topGenre = topGenre;
                                pathFileSelected = f.getAbsolutePath();
                                //Copy du fichier sans thread
                                AnalyseController.copyFile(true);

                                j++;
                            }
                        }
                    }
                    else{
                        loading=false;
                    }
                    return null;
                }
            };
            progressBar.progressProperty().bind(task.progressProperty());
            task.setOnSucceeded(new EventHandler<WorkerStateEvent>() {
                @Override
                public void handle(WorkerStateEvent event) {
                    loading=false;
                    System.out.println("FIN du chargement");
                    progressBar.progressProperty().unbind();
                    progressBar.setProgress(1.0f);
                    afficheFileInBrowser(textFieldOutput, Outputbrowser, new File(pathFileOutput), true);


                }
            });
            Thread loadthread = new Thread(task);
            loadthread.start();

        }
    }

    //Action sur le bouton de choix de dossier de sortie
    public void validPathOutput(ActionEvent actionEvent) {
        directoryChooser.setInitialDirectory(new File(pathFileOutput));
        File file=directoryChooser.showDialog(textFieldOutput.getScene().getWindow());
        if(file !=null){
            pathFileOutput=file.getAbsolutePath();
            textFieldOutput.setText(pathFileOutput);
            afficheFileInBrowser(textFieldOutput,Outputbrowser,file,true);
        }

    }

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        File file = new File(pathFile);
        File fileout=new File(pathFileOutput);
        pathFile=file.getAbsolutePath();
        pathFileOutput=fileout.getAbsolutePath();
        textFieldInput.setText(pathFile);
        textFieldOutput.setText(pathFileOutput);
        //On affiche le browser d'entrée
        afficheFileInBrowser(textFieldInput,Inputbrowser,file,false);
        //On affiche le browser de sortie
        afficheFileInBrowser(textFieldOutput,Outputbrowser,fileout,true);

        if(mainToolBar != null) {
        mainToolBar.setOnMousePressed(new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent event) {
                Stage stage = (Stage) mainToolBar.getScene().getWindow();
                xOffset = event.getSceneX();
                yOffset = event.getSceneY();
            }
        });
        mainToolBar.setOnMouseDragged(new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent event) {
                Stage stage = (Stage) mainToolBar.getScene().getWindow();
                stage.setX(event.getScreenX() - xOffset);
                stage.setY(event.getScreenY() - yOffset);
            }
        });

        }


        //Effet RGB
        DropShadow effect = (DropShadow) albumPicture.getEffect();
        effect.setColor(Color.color(1.0, 0.5, 0.5));

        rgb[0] = 0.5f;  rgbMultipliers[0] = false;
        rgb[1] = 0.5f;  rgbMultipliers[1] = true;
        rgb[2] = -1f; rgbMultipliers[2] = true;

        ScheduledExecutorService s = Executors.newSingleThreadScheduledExecutor();
        s.scheduleAtFixedRate(() -> {
           Platform.runLater(() -> {
               float[] rgbnew = rgb.clone();
               for (int i = 0; i < 3 ; i++) {
                   if(rgb[i] >= 1f){
                       rgbMultipliers[i] = false;
                   } else if (rgb[i] < -1f) {
                       rgbMultipliers[i] = true;
                   }

                   if(rgbnew[i] < 0f) rgbnew[i] = 0f;
                   if(rgbnew[i] > 1.0f) rgbnew[i] = 1.0f;

                   rgb[i]+= rgbMultipliers[i] ? COEF_RGB : -COEF_RGB;
               }
               effect.setColor(Color.color(rgbnew[0], rgbnew[1], rgbnew[2]));
           });
        }, 0, 40, TimeUnit.MILLISECONDS);
        //FIN EFFET RGB

    }

    //Permet de fermer la fenêtre et le programme
    public void handleClose(ActionEvent actionEvent) {
        ((Stage)((Button)actionEvent.getSource()).getScene().getWindow()).close();
        System.exit(0);
    }

    //Permet de mettre la fenêtre en pleine écran
    public void handleMaximize(ActionEvent actionEvent) {
        Stage window = ((Stage)((Button)actionEvent.getSource()).getScene().getWindow());
        if(window.isMaximized())
            window.setMaximized(false);
        else
            window.setMaximized(true);
    }

    //Permet de réduire la fenêtre
    public void handleMinimize(ActionEvent actionEvent){
        Stage window = ((Stage)((Button)actionEvent.getSource()).getScene().getWindow());
        if(window.isIconified())
            window.setIconified(false);
        else
            window.setIconified(true);
    }




}
