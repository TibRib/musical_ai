package fr.mAIstro.lib;

import java.io.*;
import java.util.ArrayList;
import java.util.List;

/**
 * Classe comportant uniquement des methodes statiques servant a faire des opérations  génériques sur les fichiers
 */
public class FileService {

    /**
     * Récupére le contrenu d'un fichier texte
     * @param path chemin vers le fichier a charger
     * @return contenu du fichier
     */
    public static String getFileContents(String path) {
        InputStream is;
        String fileAsString = "";
        try {
            is = new FileInputStream(path);
            @SuppressWarnings("resource")
            BufferedReader buf = new BufferedReader(new InputStreamReader(is));

            String line = buf.readLine();
            StringBuilder sb = new StringBuilder();

            while(line != null){
                sb.append(line).append("\n");
                line = buf.readLine();
            }

            fileAsString = sb.toString();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return fileAsString;
    }

    /**
     * Donne la liste des noms des fichiers se trouvant dans un dossier selon l'extension demandée
     * @param pathDir chemin du dossier
     * @param extension ne prends en compte que les fichiers avec l'extension donnée
     * @return liste de nom de fichiers
     */
    public static List<String> getListFile(String pathDir, String extension){
        File myDir = new File(pathDir);
        List<String> listmap =new ArrayList<>();
        for(String str: myDir.list()) {
            if (!extension.equals(""))
                if (str.contains("." + extension))
                    listmap.add(str);
            else
                listmap.add(str);
        }
        for (String string : listmap)
            System.out.println(string);
        return listmap;
    }
    public static List<String> getListFile(String pathDir){return getListFile(pathDir,"");}
}
